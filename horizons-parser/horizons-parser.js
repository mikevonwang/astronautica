const fs = require('fs');

const au2km = 149597870.7;
const deg2rad = Math.PI / 180;
const rad2deg = 180 / Math.PI;

const sys = {
  sol: {
    mu: 1.32712440018e11,
    rad: 695700,
    axis: {
      obli: 23.437,
      rasc: 270,
      decl: 66.563,
    },
    dir: 'pro',
    parent: 'sol',
  },
  mer: {
    mu: 2.203178e4,
    rad: 2440,
    axis: {
      obli: 0.03,
      rasc: 281.01,
      decl: 61.42,
    },
    dir: 'pro',
    parent: 'sol',
  },
  ven: {
    mu: 3.24859e5,
    rad: 6052,
    axis: {
      obli: 2.64,
      rasc: 272.76,
      decl: 67.16,
    },
    dir: 'pro',
    parent: 'sol',
  },
  ter: {
    mu: 3.986004418e5,
    rad: 6378,
    axis: {
      obli: 23.437,
      rasc: 0,
      decl: 90,
    },
    dir: 'pro',
    parent: 'sol',
  },
  lun: {
    mu: 4.9028e3,
    rad: 1738,
    axis: {
      obli: 1.54,
      rasc: 270,
      decl: 66.54,
    },
    dir: 'pro',
    parent: 'ter',
  },
  mar: {
    mu: 4.282837e4,
    rad: 3396,
    axis: {
      obli: 25.19,
      rasc: 317.68,
      decl: 52.89,
    },
    dir: 'pro',
    parent: 'sol',
  },
  jup: {
    mu: 1.26686534e8,
    rad: 71492,
    axis: {
      obli: 3.12,
      rasc: 268.06,
      decl: 64.50,
    },
    dir: 'pro',
    parent: 'sol',
  },
  iox: {
    mu: 5.926e3,
    rad: 1821,
    axis: {
      obli: 3.12,
      rasc: 268.06,
      decl: 64.50,
    },
    dir: 'pro',
    parent: 'jup',
  },
  eur: {
    mu: 3.202e3,
    rad: 1565,
    axis: {
      obli: 3.12,
      rasc: 268.06,
      decl: 64.50,
    },
    dir: 'pro',
    parent: 'jup',
  },
  gan: {
    mu: 9.9891e3,
    rad: 2634,
    axis: {
      obli: 3.12,
      rasc: 268.06,
      decl: 64.50,
    },
    dir: 'pro',
    parent: 'jup',
  },
  cal: {
    mu: 7.181e3,
    rad: 2403,
    axis: {
      obli: 3.12,
      rasc: 268.06,
      decl: 64.50,
    },
    dir: 'pro',
    parent: 'jup',
  },
  sat: {
    mu: 3.7931206159e7,
    rad: 58232,
    axis: {
      obli: 26.73,
      rasc: 40.60,
      decl: 83.54,
    },
    dir: 'pro',
    parent: 'sol',
  },
  enc: {
    mu: 7.210497,
    rad: 252,
    axis: {
      obli: 26.73,
      rasc: 40.60,
      decl: 83.54,
    },
    dir: 'pro',
    parent: 'sat',
  },
  tit: {
    mu: 8.978e3,
    rad: 2576,
    axis: {
      obli: 26.73,
      rasc: 40.60,
      decl: 83.54,
    },
    dir: 'pro',
    parent: 'sat',
  },
  ura: {
    mu: 5.793951322e6,
    rad: 25362,
    axis: {
      obli: 82.23,
      rasc: 257.43,
      decl: -15.1,
    },
    dir: 'pro',
    parent: 'sol',
  },
  nep: {
    mu: 6.8350995e6,
    rad: 24624,
    axis: {
      obli: 28.32,
      rasc: 299.36,
      decl: 43.46,
    },
    dir: 'pro',
    parent: 'sol',
  },
  tri: {
    mu: 1.432e3,
    rad: 1353,
    axis: {
      obli: 28.32,
      rasc: 299.36,
      decl: 43.46,
    },
    dir: 'ret',
    parent: 'nep',
  },
};

for (let body in sys) {
  if (!sys.hasOwnProperty(body)) { continue }
  fs.readFile(`./raw/jplData_${body}.txt`, 'utf8', (err, data) => {
    parser(data, body);
  });
}

function parser(raw,curBody) {
  raw = raw.split('\r');
  raw = raw.slice(raw.findIndex(r => r === '$$SOE')+1,raw.findIndex(r => r === '$$EOE'));
  let body = {
    elmts: [[],[],[],[],[],[],[]],
  };
  for (let datum in sys[curBody]) {
    if (!sys[curBody].hasOwnProperty(datum)) { continue }
    body[datum] = sys[curBody][datum];
  }
  for (let i=0; i<raw.length; i+=5) {
    const mu = sys[sys[curBody].parent].mu;
    const eccn = Number(raw[i+1].substring( 4,26));
    const incl = Number(raw[i+1].substring(57,78)) * deg2rad;
    const rasc = Number(raw[i+2].substring( 4,26)) * deg2rad;
    const argp = Number(raw[i+2].substring(31,52)) * deg2rad;
    const mmly = Number(raw[i+3].substring(31,52)) * deg2rad;
    const semi = Number(raw[i+4].substring( 4,26)) * au2km;
    const rada = semi * (1+eccn);
    const radp = semi * (1-eccn);
    const angm = Math.sqrt(2*mu) * Math.sqrt((rada*radp)/(rada+radp));
    const emly = mmly2emly(mmly,eccn);
    const tmly = 2 * Math.atan(Math.sqrt((1+eccn)/(1-eccn))*Math.tan(emly/2));
    const radi = (Math.pow(angm,2) / mu) / (1 + eccn*Math.cos(tmly));
    const para = semi * (1 - Math.pow(eccn,2));

    /*body.elmts[0].push(Number(((angm * eccn) / (radi * para)).toPrecision(8)));
    body.elmts[1].push(Number((angm / radi).toPrecision(8)));
    body.elmts[2].push(Number((Math.cos(rasc) * Math.cos(incl)).toPrecision(8)));
    body.elmts[3].push(Number((Math.sin(rasc) * Math.cos(incl)).toPrecision(8)));
    body.elmts[4].push(Number((Math.sin(incl)).toPrecision(8)));
    body.elmts[5].push(Number((argp).toPrecision(8)));
    body.elmts[6].push(Number((tmly).toPrecision(8)));*/
    body.elmts[0].push(Number((angm).toPrecision(8)));
    body.elmts[1].push(Number((eccn).toPrecision(8)));
    body.elmts[2].push(Number((semi).toPrecision(8)));
    body.elmts[3].push(Number((rasc).toPrecision(8)));
    body.elmts[4].push(Number((incl).toPrecision(8)));
    body.elmts[5].push(Number((argp).toPrecision(8)));
    body.elmts[6].push(Number((tmly).toPrecision(8)));
  }
  const eclAxis = equ2ecl([sys[curBody].axis.rasc * deg2rad,sys[curBody].axis.decl * deg2rad]);
  body.axis.rasc = eclAxis[0];
  body.axis.decl = eclAxis[1];
  body.grid = {
    lat: [],
    lon: [],
  }
  for (let phi=30; phi<=150; phi+=30) {
    var lat_line = [];
    for (let theta=0; theta<=360; theta+=5) {
      const p = phi * deg2rad;
      const t = theta * deg2rad;
      let lat = [
        body.rad * Math.sin(p) * Math.cos(t),
        body.rad * Math.sin(p) * Math.sin(t),
        body.rad * Math.cos(p),
      ];
      lat = [
        Number((Math.cos((90-body.axis.decl)*deg2rad) * lat[0] + Math.sin((90-body.axis.decl)*deg2rad) * lat[2]).toPrecision(8)),
        Number((lat[1]).toPrecision(8)),
       Number((-Math.sin((90-body.axis.decl)*deg2rad) * lat[0] + Math.cos((90-body.axis.decl)*deg2rad) * lat[2]).toPrecision(8)),
      ];
      lat = [
        Number((Math.cos(body.axis.rasc*deg2rad) * lat[0] - Math.sin(body.axis.rasc*deg2rad) * lat[1]).toPrecision(8)),
        Number((Math.sin(body.axis.rasc*deg2rad) * lat[0] + Math.cos(body.axis.rasc*deg2rad) * lat[1]).toPrecision(8)),
        Number((lat[2]).toPrecision(8)),
      ];
      lat_line.push(lat);
    }
    body.grid.lat.push(lat_line);
  }
  for (let theta=0; theta<360; theta+=30) {
    let lon_line = [];
    for (let phi=0; phi<=180; phi+=5) {
      const p = phi * deg2rad;
      const t = theta * deg2rad;
      let lon = [
        body.rad * Math.sin(p) * Math.cos(t),
        body.rad * Math.sin(p) * Math.sin(t),
        body.rad * Math.cos(p),
      ];
      lon = [
        Number((Math.cos((90-body.axis.decl)*deg2rad) * lon[0] + Math.sin((90-body.axis.decl)*deg2rad) * lon[2]).toPrecision(8)),
        Number((lon[1]).toPrecision(8)),
       Number((-Math.sin((90-body.axis.decl)*deg2rad) * lon[0] + Math.cos((90-body.axis.decl)*deg2rad) * lon[2]).toPrecision(8)),
      ];
      lon = [
        Number((Math.cos(body.axis.rasc*deg2rad) * lon[0] - Math.sin(body.axis.rasc*deg2rad) * lon[1]).toPrecision(8)),
        Number((Math.sin(body.axis.rasc*deg2rad) * lon[0] + Math.cos(body.axis.rasc*deg2rad) * lon[1]).toPrecision(8)),
        Number((lon[2]).toPrecision(8)),
      ];
      lon_line.push(lon);
    }
    body.grid.lon.push(lon_line);
  }
  fs.writeFile(`../dist/system/${curBody}.js`, 'const ' + curBody + ' = ' + JSON.stringify(body) + ';', 'utf8', () => {});
}

function equ2ecl(input) { // convert from earth equatorial (celestial) coordinates to ecliptic
  const equ = {
    x: Math.cos(input[1]) * Math.cos(input[0]),
    y: Math.cos(input[1]) * Math.sin(input[0]),
    z: Math.sin(input[1]),
  };
  const obli = sys['ter'].axis.obli * deg2rad;
  const ecl = {
    x: equ.x,
    y: Math.cos(obli) * equ.y + Math.sin(obli) * equ.z,
    z: -Math.sin(obli) * equ.y + Math.cos(obli) * equ.z,
  };
  return ([
    Math.atan2(ecl.y,ecl.x) * rad2deg,
    90 - Math.atan2(Math.sqrt(Math.pow(ecl.x,2) + Math.pow(ecl.y,2)), ecl.z) * rad2deg,
  ]);
}

function mmly2emly(mmly, eccn) { // convert from mean anomaly to eccentric anomaly
  const tol = 1e-8;
  let emly;
  if (mmly > Math.PI) {
    emly = mmly - eccn/2;
  }
  else {
    emly = mmly + eccn/2;
  }
  let dist = 1;
  while (dist > tol) {
    const f_over_fp = (emly - eccn*Math.sin(emly) - mmly) / (1 - eccn*Math.cos(emly));
    dist = Math.abs(f_over_fp);
    emly = emly - f_over_fp;
  }
  return (emly)
}
