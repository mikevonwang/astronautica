var package = require('./package.json');

var version_string = package.version.replace(/\./g,'-');

module.exports = {
  filename_js: `bundle.${version_string}.js`,
  filename_css: `index.${version_string}.css`,
};
