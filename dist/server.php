<?php

  namespace Mieda;

  error_reporting(E_ERROR | E_PARSE);

  header('Access-Control-Allow-Origin: http://localhost:9093');
  header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
  header('Access-Control-Allow-Headers: Content-Type, Authorization');

  date_default_timezone_set('America/Chicago');

  require_once('php/vendor/JWT.php');
  require_once('php/vendor/password.php');
  require_once('php/vendor/PHPMailer/Exception.php');
  require_once('php/vendor/PHPMailer/PHPMailer.php');
  require_once('php/vendor/PHPMailer/SMTP.php');

  require_once('php/mieda/constant.php');
  require_once('php/mieda/email.php');
  require_once('php/mieda/controller.php');
  require_once('php/mieda/router.php');

  require_once('php/global/endpoints.php');
  require_once('php/global/constants.php');
  require_once('php/global/app.php');

  $pos_q_mark = strpos($_SERVER['REQUEST_URI'], '?');
  if ($pos_q_mark == false) {
    $uri = explode('/', substr($_SERVER['REQUEST_URI'], 5));
    $queries = '';
  }
  else {
    $uri = explode('/', substr($_SERVER['REQUEST_URI'], 5, $pos_q_mark - 5));
    $queries = substr($_SERVER['REQUEST_URI'], $pos_q_mark + 1);
  }
  $version = $uri[0];
  $method = $_SERVER['REQUEST_METHOD'];
  $output = NULL;
  $env = NULL;
  $endpoint = array_slice($uri, 1);
  $headers = (array) apache_request_headers();

  $config = json_decode(file_get_contents('config.json'), true);

  if ($method != 'OPTIONS') {

    // establish a connection to the database
    switch ($_SERVER['SERVER_NAME']) {
      case $config['env']['local']['server_name']:
        $env = 'local';
      break;
      case $config['env']['live']['server_name']:
        $env = 'live';
      break;
    }
    $con = mysqli_connect(
      $config['env'][$env]['db']['host'],
      $config['env'][$env]['db']['user'],
      $config['env'][$env]['db']['pass'],
      $config['env'][$env]['db']['name']
    );

    if ($con && mysqli_set_charset($con, 'utf8')) {

      $match = $router->match($method, $endpoint);

      if ($method == 'GET') {
        $data = [];
        if ($queries != '') {
          $data_pairs = explode('&', $queries);
          foreach($data_pairs as $pair) {
            $pair_array = explode('=', $pair);
            $data[$pair_array[0]] = $pair_array[1];
          }
        }
        $data = array_replace($data, $match['params']);
      }
      else {
        $data = json_decode(file_get_contents('php://input'));
        $data = (array) $data;
        $data = array_replace($data, $match['params']);
      }
      if (isset($headers['Authorization']) && substr($headers['Authorization'], 0, 6) == 'Bearer') {
        $data['token'] = substr($headers['Authorization'], 7);
      }

      if (is_null($match['handler'])) {
        $output = ['err' => 'nonexistent_endpoint', 'result' => NULL];
        http_response_code(404);
      }
      else {
        Controller::init($data, $con, $env, $config, $const);
        if ($match['protected'] == true) {
          Controller::check_token();
        }
        else {
          Controller::get_token();
        }
        $output = call_user_func('\Mieda\\' . $match['handler']);
      }

      // log queries
      if ($env == 'local' && !is_null($match['handler']) && isset($output['qry']) && !is_null($output['qry'])) {
        file_put_contents('php://stdout', "\n" . '- - - - - - - - - - - - - - - - - -' . "\n");
        file_put_contents('php://stdout', "\n" . date('d M Y H:i:s') . ' - - - ' . $method . ' /' . join('/', $endpoint) . "\n");
        file_put_contents('php://stdout', "\n" . '- - - - - - - - - - - - - - - - - -' . "\n");
        foreach($output['qry'] as $q) {
          file_put_contents('php://stdout', "\n" . '>>  ' . $q . "\n");
        }
        file_put_contents('php://stdout', "\n\n\n\n");
      }

      if (!is_null($match['handler']) && isset($output['qry']) && !is_null($output['qry'])) {
        unset($output['qry']);
      }

      mysqli_close($con);
    }
    else {
      $output = ['err' => 'connection_error', 'result' => NULL];
      http_response_code(502);
    }

    // return data to network.js
    echo json_encode($output);
    exit();
  }

?>
