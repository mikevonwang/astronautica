<?php

  namespace Mieda;

  global $jwt_key;

  abstract class Controller {

    protected static $err;
    protected static $result;
    protected static $temp;
    protected static $qry;
    protected static $ans;
    protected static $data;
    protected static $con;
    protected static $env;
    protected static $const;
    protected static $config;

    public static function init($data, $con, $env, $config, $const) {
      self::$con = $con;
      self::$err = NULL;
      self::$result = NULL;
      self::$temp = [];
      self::$qry = [];
      self::$ans = NULL;
      self::$data = self::sanitize($data);
      self::$env = $env;
      self::$const = $const;
      self::$config = $config;
    }

    private static function sanitize($data) {
      return array_map(function($value) {
        if (is_array($value)) {
          return self::sanitize($value);
        }
        else if (is_object($value)) {
          return self::sanitize((array) $value);
        }
        else if (is_null($value)) {
          return null;
        }
        else {
          return mysqli_real_escape_string(self::$con, $value);
        }
      }, $data);
    }

    protected static function c() {
      if (is_null(self::$err)) {
        self::$ans = NULL;
        return true;
      }
      else {
        return false;
      }
    }

    protected static function output() {
      if (!(self::c())) {
        self::$result = NULL;
      }
      $output = [
        'err' => self::$err,
        'result' => self::$result,
        'qry' => self::$qry
      ];
      return $output;
    }

    protected static function query($qry_raw) {
      $qry = join(' ', $qry_raw);
      self::$qry[] = $qry;
      self::$ans = mysqli_query(self::$con, $qry);
      if (!self::$ans) {
        if (self::$env === 'local') {
          self::$err = mysqli_error(self::$con);
        }
        else {
          self::$err = 'server_error';
        }
        http_response_code(500);
      }
      return self::$ans;
    }

    public static function check_token() {
      if (self::c()) {
        if (!isset(self::$data['token'])) {
          self::$err = 'invalid_token';
          http_response_code(401);
        }
      }
      if (self::c()) {
        try {
          $decoded = \JWT::decode(self::$data['token'], self::$config['jwt_key']);
          self::$temp = array(
            'token_user_id' => $decoded->user_id,
            'token_username' => $decoded->username,
          );
        }
        catch (\Exception $e) {
          self::$err = 'invalid_token';
          http_response_code(401);
        }
      }
      if (self::c()) {
        self::query([
          'UPDATE users',
          'SET date_activity = NOW()',
          'WHERE user_id = "' . self::$temp['token_user_id'] . '"',
        ]);
      }
    }

    public static function get_token() {
      if (isset(self::$data['token'])) {
        if (self::c()) {
          try {
            $decoded = \JWT::decode(self::$data['token'], self::$config['jwt_key']);
            self::$temp = array(
              'token_user_id' => $decoded->user_id,
              'token_username' => $decoded->username,
            );
          }
          catch (\Exception $e) {
          }
        }
        if (self::c()) {
          if (self::$temp['token_user_id']) {
            self::query([
              'UPDATE users',
              'SET date_activity = NOW()',
              'WHERE user_id = "' . self::$temp['token_user_id'] . '"',
            ]);
          }
        }
      }
    }

    public static function log($output) {
      file_put_contents('php://stdout', "\n" . print_r($output,true) . "\n");
    }

    protected static function get_external_url($url, $timeout = 5) {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $html = curl_exec($curl);
      if ($html) {
        return $html;
      }
      else {
        return false;
      }
    }

    protected static function check_id_validity($id) {
      return (!preg_match('/\D/', $id));
    }

    public static function any_value($table, $columns) {
      return join(', ', array_map(function($column) use ($table) {
        return ('ANY_VALUE(' . $table . '.' . $column . ') AS ' . $column);
      }, $columns));
    }

    public static function send_admin_mail($subject, $body) {
      try {
        $url = self::$config['env'][self::$env]['app_url'];

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

        $mail->isSMTP();
        $mail->Host =       self::$config['email']['host'];
        $mail->SMTPSecure = self::$config['email']['security'];
        $mail->Port =       self::$config['email']['port'];
        $mail->SMTPAuth =   true;
        $mail->Username =   self::$config['email']['username'];
        $mail->Password =   self::$config['email']['password'];

        $mail->setFrom(self::$config['email']['from']['email'], self::$config['email']['from']['name']);
        $mail->addAddress(self::$config['email']['recipient']);

        $mail->Subject = $subject;

        $mail->Body = $body;

        $mail->Send();
      }
      catch (\Exception $e) {
        self::$err = 'mailer_failed';
        http_response_code(500);
      }
    }


    public static function send_mail($recipient, $subject, $body) {
      try {
        global $config;

        $url = '';
        switch ($_SERVER['SERVER_NAME']) {
          case $config['env']['local']['server_name']:
            $url = $config['env']['local']['app_url'];
          break;
          case $config['env']['staging']['server_name']:
            $url = $config['env']['staging']['app_url'];
          break;
          case $config['env']['live']['server_name']:
            $url = $config['env']['live']['app_url'];
          break;
        }

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

        $mail->isSMTP();
        $mail->Host =       $config['email']['host'];
        $mail->SMTPSecure = $config['email']['security'];
        $mail->Port =       $config['email']['port'];
        $mail->SMTPAuth =   true;
        $mail->Username =   $config['email']['username'];
        $mail->Password =   $config['email']['password'];

        $mail->setFrom($config['email']['from']['email'], $config['email']['from']['name']);
        $mail->addAddress($recipient);

        $mail->isHTML(true);
        $mail->Subject = $subject;

        $mail->Body = (new Email($recipient, $subject, $body, $url))->get_html();

        $mail->Send();
      }
      catch (\Exception $e) {
        self::$err = 'mailer_failed';
        http_response_code(500);
      }
    }

    public static function base64_url_encode($input) {
      return strtr(base64_encode($input), '+/=', '._-');
    }

    public static function base64_url_decode($input) {
      return base64_decode(strtr($input, '._-', '+/='));
    }

    public static function slugify($input) {
      return preg_replace('/[^A-Za-z0-9_]+/', '_', transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", trim($input)));
    }

    public static function generate_id($n) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $output = '';
      for ($i=0; $i<$n; $i++) {
        $output .= $characters[rand(0, strlen($characters) - 1)];
      }
      return $output;
    }

  }

?>
