<?php

  namespace Mieda;

  class Ephemeris extends Controller {

    public static function get() {
      if (self::c()) {
        $options = [
          'format' => 'text',
          'EPHEM_TYPE' => 'ELEMENTS',
          'CENTER' => self::$data['CENTER'],
          'STEP_SIZE' => self::$data['STEP_SIZE'],
          'START_TIME' => self::$data['START_TIME'],
          'STOP_TIME' => self::$data['STOP_TIME'],
          'COMMAND' => self::$data['COMMAND'],
        ];
        $ch = curl_init('https://ssd.jpl.nasa.gov/api/horizons.api?' . http_build_query($options));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $receivedData = curl_exec($ch);
        self::$result = [
          'data' => $receivedData,
        ];
      }
      return self::output();
    }

  }

?>
