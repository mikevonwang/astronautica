var gulp = require('gulp');
var tasks = {};

var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var webpack = require('webpack-stream');
var sftp = require('gulp-sftp');
var rename = require('gulp-rename');
var fs = require('fs');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var rimraf = require('rimraf');

var config = require('./config').full;
var config_dist = require('./config').dist;
var version = require('./version.js');

// - - - - -
// compile SCSS files into CSS
// - - - - -
function task_scss() {
  return gulp.src('src/global/scss/index.scss')
  .pipe(rename(version.filename_css))
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('dist/gen'))
  .pipe(notify({message: 'scss - ' + new Date().toTimeString().substring(0,8), onLast: true}));
};

// - - - - -
// compile ES6+ files into JS
// - - - - -
function task_js(mode) {
  var destination_folder;
  var webpack_file;
  switch (mode) {
    case 'dev':
      destination_folder = 'dist/gen';
      webpack_file = './webpack.dev.js';
    break;
    case 'prod':
      destination_folder = `${config.gulp_temp_path}/gen`;
      webpack_file = './webpack.prod.js';
    break;
  }

  var html = fs.readFileSync('./dist/index.html', 'utf8');
  html = html.replace(/\/bundle.*?\.js/, `/${version.filename_js}`);
  html = html.replace(/\/index.*?\.css/, `/${version.filename_css}`);
  fs.writeFileSync('./dist/index.html', html);

  return gulp.src('src/global/js/app.js')
  .pipe(sourcemaps.init())
  .pipe(webpack(require(webpack_file)))
  .on('error', function(err) {
    console.error('[Compilation Error]');
    console.error(err.message + '\n');
    console.error(err.codeFrame);
    this.emit('end');
  })
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(destination_folder))
  .pipe(notify({message: 'js - ' + new Date().toTimeString().substring(0,8), onLast: true}));
};
function task_js_dev() {
  return task_js('dev');
};
function task_js_prod() {
  return task_js('prod')
};


// - - - - -
// clear generated folder
// - - - - -
gulp.task('clear', (done) => {
  rimraf.sync('./dist/gen');
  done();
});


// - - - - -
// grab polyfills from the "babel-polyfill" node module
// - - - - -
function task_polyfill() {
  return gulp.src('node_modules/@babel/polyfill/dist/polyfill.min.js')
  .pipe(gulp.dest('dist'));
}


// - - - - -
// grab the database's schema
// - - - - -
function task_database(done) {
  var mysql_command = 'mysqldump --add-drop-table --no-data -u ' + config.env.local.db.user + ' -p' + config.env.local.db.pass + ' ' + config.env.local.db.name + ' ';
  var sed_1 = '| sed "s/ AUTO_INCREMENT=[0-9]*//g" ';
  var sed_2 = '| sed "s/-- Dump completed on .*"//g ';
  exec(mysql_command + sed_1 + sed_2 + '> ' + process.cwd() + '/database.sql');
  done();
}


// - - - - -
// config tasks
// - - - - -
gulp.task('config', gulp.parallel(task_config_schema, task_config_full, task_config_dist));


// - - - - -
// write config schema
// - - - - -
function get_schema(input) {
  let output = {};
  Object.keys(input).forEach((key) => {
    if (typeof input[key] === 'object' && input[key] !== null) {
      output[key] = get_schema(input[key]);
    }
    else {
      output[key] = '';
    }
  });
  return output;
}
function task_config_schema(done) {
  const schema = {
    dist: get_schema(config_dist),
    full: get_schema(config),
  }
  fs.writeFile('./config-schema.json', JSON.stringify(schema), (err) => {
    if (err) {
      return console.log(err);
    }
    done();
  });
}


// - - - - -
// write config as JSON for server
// - - - - -
function task_config_full(done) {
  fs.writeFile('dist/config.json', JSON.stringify(config), (err) => {
    if (err) {
      return console.log(err);
    }
    done();
  });
}

// - - - - -
// write config as JSON for webapp
// - - - - -
function task_config_dist(done) {
  fs.writeFile('./config-dist.json', JSON.stringify(config_dist), (err) => {
    if (err) {
      return console.log(err);
    }
    done();
  });
}


// - - - - -
// start filewatchers on SCSS and ES6+ files
// - - - - -
function task_watch() {
  gulp.watch('src/**/*.scss', gulp.parallel(task_scss));
  gulp.watch(['src/**/*.js', 'config.js'], gulp.series(
    'config',
    gulp.parallel(task_js_dev, task_database)
  ));
}


// - - - - -
// build for local environment
// - - - - -
gulp.task('default', gulp.series(
  'config',
  'clear',
  gulp.parallel(task_scss, task_js_dev, task_database, task_polyfill, task_watch)
));


// - - - - -
// copy files from dist to temp folder
// - - - - -
function copy_dist_to_temp() {
  return gulp.src(['dist/**', 'dist/.htaccess', '!dist/bundle.js', '!dist/**/*.map', '!dist/db/words.json'])
  .pipe(gulp.dest(config.gulp_temp_path));
}


// - - - - -
// delete temp folder
// - - - - -
function delete_temp() {
  return exec('rm -rf ' + config.gulp_temp_path);
}


// - - - - -
// build and deploy to production environment
// - - - - -
function publish_sftp() {
  return gulp.src([config.gulp_temp_path + '**', config.gulp_temp_path + '**/.*'])
  .pipe(sftp({
    host: config.env.live.IP,
    user: config.env.live.sftp.user,
    remotePath: config.env.live.sftp.remote_path,
  }));
}
gulp.task('publish_build', gulp.series(
  'config',
  'clear',
  gulp.parallel(task_scss, task_js_prod, task_polyfill),
  copy_dist_to_temp
));
gulp.task('publish', gulp.series(gulp.parallel('publish_build'), publish_sftp, delete_temp));


// - - - - -
// backup database and uploads from remote environment
// - - - - -
async function backup_sftp() {
  return spawn('sftp', ['-r', config.env.live.sftp.user + '@' + config.env.live.IP + ':' + config.env.live.db_path + '/words.json', config.env.local.db_path], {stdio: 'inherit'});
}
gulp.task('backup', gulp.series(backup_sftp));
