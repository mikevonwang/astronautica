import { ToastContainer } from 'react-toastify';

import { xhr } from './xhr';
import Utilities from './utilities';

import About from '../../pages/About/About';

import Main from '../../pages/Main/Main';
import Mission from '../../pages/Mission/Mission';
import Sandbox from '../../pages/Sandbox/Sandbox';
import Error404 from '../../pages/Error404/Error404';

import 'react-toastify/dist/ReactToastify.css';

function App(props) {

  const [env, setEnv] = React.useState(null);
  const [root, setRootActual] = React.useState({
    masked: false,
  });

  React.useEffect(() => {
    switch (location.hostname) {
      case Config.env.local.location_hostname:
        setEnv('local');
      break;
      case Config.env.live.location_hostname:
        setEnv('live');
        console.log = () => {};
      break;
    }
  }, []);

  function setRoot(new_root) {
    setRootActual((old_root) => {
      return ({
        ...old_root,
        ...new_root
      });
    });
  }

  return (
    <>
      {Rhaetia.renderChild(props.children, {
        root: root,
        setRoot: setRoot,
      })}
      <div className={'mask ' + (root.masked ? 'active' : '')}/>
      <ToastContainer autoClose={4000} hideProgressBar={true}/>
    </>
  );

};

const route_tree = [
  ['', Main, [
    ['', Mission, [
      ['', Sandbox],
    ]],
  ]],
  ['about', About],
];

ReactDOM.render((
  <Rhaetia.Router routeTree={route_tree} page404={Error404} fallbackURL={'/'}>
    <App/>
  </Rhaetia.Router>
), document.getElementById('root'));
