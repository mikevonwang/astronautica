export function useInterval(callback, delay) {
  const savedCallback = React.useRef();

  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  React.useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export function hasForbiddenCharacters(string) {
  return (!string.match(/^[a-zA-Z0-9_-]+$/));
}

export function displayDate(fullDate) {
  const month = `0${fullDate.getMonth()+1}`.slice(-2);
  const date = `0${fullDate.getDate()}`.slice(-2);
  return `${fullDate.getFullYear()}-${month}-${date}`;
}

export function setQueryString(router, key, value) {
  const searchParams = new URLSearchParams(window.location.search);
  searchParams.set(key, value);
  const isBlocked = router.isBlocked();
  if (isBlocked) {
    router.unblock();
  }
  router.replace(window.location.pathname + '?' + searchParams.toString());
  if (isBlocked) {
    router.block();
  }
}

export function sort(list, selectors, options = {}) {
  const {
    direction = 'asc',
    isNullLarge = false,
  } = options;
  function sortBySelectorsRecursively(a, b, i) {
    if (selectors[i]) {
      const A = selectors[i](a);
      const B = selectors[i](b);
      if (A !== null && B === null) {
        return ((direction === 'asc') === (isNullLarge === false)) ? 1 : -1;
      }
      else if (A === null && B !== null) {
        return ((direction === 'asc') === (isNullLarge === false)) ? -1 : 1;
      }
      else if (A > B) {
        return (direction === 'asc') ? 1 : -1;
      }
      else if (A < B) {
        return (direction === 'asc') ? -1 : 1;
      }
      else {
        return sortBySelectorsRecursively(a, b, i+1);
      }
    }
    else {
      return 0;
    }
  }
  return list.slice().sort((a, b) => sortBySelectorsRecursively(a, b, 0));
}
