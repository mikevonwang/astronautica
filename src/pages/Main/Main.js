import { xhr } from '../../global/js/xhr';

import ModalConfirm from '../../modules/ModalConfirm/ModalConfirm';
import Spinner from '../../modules/Spinner/Spinner';

export default function Main(props) {

  const [modal, setModal] = React.useState(null);
  const [spinner, setSpinner] = React.useState(false);

  function onClickBlackout() {
    if (modal.is_forced !== true) {
      setModal(null);
    }
  }

  let Modal = null;
  let class_modal_active = '';
  if (modal !== null) {
    switch (modal.type) {
      case 'confirm':
        Modal = ModalConfirm; break;
    }
    Modal = (
      <Modal
        modal={modal}
        root={root}
        setModal={setModal}
        setRoot={props.setRoot}
        router={props.router}
      />
    );
    class_modal_active = ' active';
  }

  return (
    <>
      <header className="main">
        <div className="header-left">
          <A href="/" className="logo" onClick={() => location.reload()}><img src="/img/logo_mini.png"/></A>
        </div>
        <div className="header-right">
          <A className="header-link" href="/about"><span>About</span></A>
        </div>
      </header>
      {Rhaetia.renderChild(props.children, {
        root: props.root,
        setRoot: props.setRoot,
        setModal: setModal,
        setSpinner: setSpinner,
      })}
      <Spinner
        is_active = {spinner}
      />
      <section className={'modal' + class_modal_active}>
        <div className='blackout' onClick={onClickBlackout}></div>
        {Modal}
      </section>
    </>
  );

}
