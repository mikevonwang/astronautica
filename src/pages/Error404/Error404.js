export default function Error404(props) {

  return (
    <div className="error404">
      <h2>We couldn't find what you were looking for! 😬</h2>
      <A href="/">Back to home</A>
    </div>
  )

}
