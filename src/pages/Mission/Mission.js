import * as Framework from '../../core/Framework';

export default function Mission(props) {

  const [size, setSize] = React.useState(getDisplaySize());
  const [mission, setMissionActual] = React.useState({
    ...getDefaultStartAndEnd(),
    included_bodies: getDefaultIncludedBodies(),
    frame_interval: getDefaultFrameInterval(),
  });
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {
    for (let body in sys) {
      Framework.assembleWireframes(body);
    }
  }, []);

  function getDisplaySize() {
    let sizeNum;
    const heightLimit = window.innerHeight - 125; // 36 + 2*20 + 25 + 10
    if (window.innerWidth > 1400) {
      sizeNum = window.innerWidth - (2 * 340);
      if (sizeNum > heightLimit) {
        sizeNum = heightLimit;
      }
    }
    else {
      sizeNum = heightLimit;
    }
    return sizeNum;
  }

  function getDefaultStartAndEnd() {
    const startRaw = new Date(Number(props.router.query.start + '000'));
    const endRaw = new Date(Number(props.router.query.end + '000'));
    let start;
    let end;
    if (!isNaN(startRaw.getTime())) {
      start = startRaw;
      if (!isNaN(endRaw.getTime())) {
        end = endRaw;
      }
      else {
        end = new Date(start);
        end.setFullYear(end.getFullYear() + 1);
      }
    }
    else {
      start = new Date();
      end = new Date(start);
      end.setFullYear(end.getFullYear() + 1);
    }
    return {
      start_day: Math.floor(start.getTime() / 1000),
      end_day: Math.floor(end.getTime() / 1000),
    };
  }

  function getDefaultFrameInterval() {
    const n = Number(props.router.query.frameInterval);
    return !isNaN(n) ? n : 21600;
  }

  function getDefaultIncludedBodies() {
    const rawText = props.router.query.includedBodies;
    const defaultIncludedBodies = {
      planets: true,
      mars: false,
      jupiter: false,
      saturn: false,
      uranus: false,
      neptune: false,
      asteroids: false,
      kuiper: false,
    };
    const includedBodies = {
      ...defaultIncludedBodies,
    };
    try {
      const object = JSON.parse(decodeURIComponent(rawText));
      Object.keys(defaultIncludedBodies).forEach(key => {
        if (typeof object[key] === 'boolean') {
          includedBodies[key] = object[key];
        }
      });
    }
    catch(err) {

    }
    return includedBodies;
  }

  function setMission(newMissionData) {
    setMissionActual({
      ...mission,
      ...newMissionData,
    });
  }

  return (
    <div className="mission">
      {props.children && React.cloneElement(props.children, {
        size: size,
        user: props.root.user,
        mission: mission,
        setMission: setMission,
        isLoading: isLoading,
      })}
    </div>
  );

}
