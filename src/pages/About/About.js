import LegalFooter from '../../modules/LegalFooter/LegalFooter';

export default function About(props) {

  return (
    <main className='legal about'>
      <a href='/'><img src={'/img/logo.png'}/></a>
      <h1>About Astronautica</h1>
      <section>
        <h2>History</h2>
        <p>The project that would one day become Astronautica came to life in 2015 as a simple Earth-Luna trajectory plotter. It was written in Matlab, and took several seconds to process a simple free return mission. After a detour through the world of C++, Astronautica settled into its current home of Javascript in 2017.</p>
        <p>Features were slowly added and removed over the next few years. In 2020, Astronautica arrived at its current domain name, and settled on its current focus of Solar System visualization in 2024.</p>
      </section>
      <section>
        <h2>How it works</h2>
        <p>Astronautica gets its data (i.e. ephemerides) from the <a href="https://ssd.jpl.nasa.gov/horizons/app.html#/">JPL Horizons</a> system. It uses <a href="https://www.math.ntnu.no/emner/TMA4215/2008h/cubicsplines.pdf">natural cubic splines</a> to interpolate between the <a href="https://en.wikipedia.org/wiki/Osculating_orbit">osculating orbital elements</a> it receives to plot the positions of Solar System bodies in three dimensions with high accuracy.</p>
      </section>
      <LegalFooter/>
    </main>
  );

}
