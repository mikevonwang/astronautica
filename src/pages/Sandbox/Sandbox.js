import { toast } from 'react-toastify';

import { xhr } from '../../global/js/xhr';
import * as Framework from '../../core/Framework';
import * as Utilities from '../../global/js/utilities';

import InputText from '../../modules/InputText/InputText';
import InputNum from '../../modules/InputNum/InputNum';
import InputDuration from '../../modules/InputDuration/InputDuration';
import InputSel from '../../modules/InputSel/InputSel';
import InputCheck from '../../modules/InputCheck/InputCheck';
import OutputText from '../../modules/OutputText/OutputText';
import Spinner from '../../modules/Spinner/Spinner';
import OrbitPlot from '../../modules/OrbitPlot/OrbitPlot';

const viewOptions = ['+x','-x','+y','-y','+z','-z'];

const scaleRange = [3.5, 9.8];
const playbackSpeedRange = [1, 10];
const qualityRange = [1, 50];

export default function Sandbox(props) {

  const [kinematics, setKinematics] = React.useState(null);
  const [postKinematics, setPostKinematics] = React.useState(null);
  const [cam, setCam] = React.useState(getDefaultCam());
  const [origin, setOrigin] = React.useState(getDefaultOrigin());
  const [scale, setScale] = React.useState(getDefaultScale());
  const [quality, setQuality] = React.useState(getDefaultQuality());
  const [frame, setFrame] = React.useState(0);
  const [jump, setJump] = React.useState(getDefaultJump());
  const [timeoutCutoff, setTimeoutCutoff] = React.useState(getDefaultTimeoutCutoff());
  const [futureOrbitRenderMode, setFutureOrbitRenderMode] = React.useState(getDefaultFutureOrbitRenderMode());
  const [isPlaying, setIsPlaying] = React.useState(false);
  const [isRendering, setIsRendering] = React.useState(true);

  const availableOrigins = getAvailableOrigins();

  React.useEffect(() => {
    if (!availableOrigins.find(option => option[0] === origin)) {
      setOrigin(sys[origin].parent);
    }
  }, [JSON.stringify(Object.keys(kinematics?.orbits || {}))]);

  function getDefaultCam() {
    const azi = Number(props.router.query.camAzi);
    const alt = Number(props.router.query.camAlt);
    return {
      azi: !isNaN(azi) ? azi : 0,
      alt: !isNaN(alt) ? alt : 0,
    };
  }

  function getDefaultOrigin() {
    return (props.router.query.origin && Object.keys(sys).some(key => key === props.router.query.origin)) ? props.router.query.origin : 'bar';
  }

  function getDefaultScale() {
    const n = Number(props.router.query.scale);
    if (!isNaN(n)) {
      const l = Math.log10(n);
      if (l >= scaleRange[0] && l <= scaleRange[1]) {
        return n;
      }
    }
    return 1e8;
  }

  function getDefaultQuality() {
    const n = Number(props.router.query.quality);
    if (!isNaN(n)) {
      if (n >= qualityRange[0] && n <= qualityRange[1]) {
        return 51 - n;
      }
    }
    return 1;
  }

  function getDefaultTimeoutCutoff() {
    const n = Number(props.router.query.timeout);
    if (!isNaN(n)) {
      return n;
    }
    return 5;
  }

  function getDefaultJump() {
    const n = Number(props.router.query.speed);
    if (!isNaN(n)) {
      if (n >= playbackSpeedRange[0] && n <= playbackSpeedRange[1]) {
        return n;
      }
    }
    return 1;
  }

  function getDefaultFutureOrbitRenderMode() {
    if (props.router.query.futureOrbitRenderMode === 'one') {
      return 'one';
    }
    else if (props.router.query.futureOrbitRenderMode === 'all') {
      return 'all';
    }
    return 'all';
  }

  React.useEffect(() => {
    calculateTrajectory();
  }, []);

  function incrementVideo(value) {
    if (value === -1 && frame > 0) {
      setFrame(frame - 1);
    }
    else if (value === 1 && frame < kinematics.MET.length - 1) {
      setFrame(frame + 1);
    }
    if (isPlaying !== false) {
      setIsPlaying(false);
    }
  }

  function playPauseVideo() {
    if (isPlaying === false) {
      if (frame === kinematics.MET.length - 1) {
        setFrame(0);
      }
      setIsPlaying(true);
    }
    else {
      setIsPlaying(false);
    }
  }

  function stopVideo() {
    setFrame(0);
    setIsPlaying(false);
  }

  Utilities.useInterval(() => {
    if (kinematics) {
      let newFrame = frame + jump;
      if (newFrame >= kinematics.MET.length) {
        newFrame = kinematics.MET.length-1;
        setFrame(newFrame);
        setIsPlaying(false);
      }
      else {
        setFrame(newFrame);
      }
    }
  }, isPlaying ? 20 : null);

  function setBurn(i, key, value) {
    props.setMission({
      burns: props.mission.burns.map((burn, j) => {
        if (i === j) {
          return {
            ...burn,
            [key]: value,
          };
        }
        else {
          return burn;
        }
      })
    });
  }

  function setBurnAttitude(i, a, value) {
    props.setMission({
      burns: props.mission.burns.map((burn, j) => {
        if (i === j) {
          return {
            ...burn,
            attitude: burn.attitude.map((component, b) => {
              if (a === b) {
                return value;
              }
              else {
                return component;
              }
            }),
          };
        }
        else {
          return burn;
        }
      })
    });
  }

  function removeBurn(i) {
    props.setMission({
      burns: props.mission.burns.filter((burn, j) => {
        return (j !== i);
      }),
    });
  }

  function addBurn() {
    props.setMission({
      burns: [
        ...props.mission.burns,
        {
          sMET: 0,
          duration: 10,
          engine: 0,
          lock: true,
          throttle: 1,
          attitude: [1, 0, 0],
        },
      ],
    });
  }

  async function getHorizonsEphemerides() {
    const missionStartDate = new Date(props.mission.start_day * 1000);
    const missionEndDate = new Date((props.mission.end_day + 86400) * 1000);

    const missionLength = (missionEndDate - missionStartDate) / 1000 / 86400;
    let ephemerisIntervalRaw = Math.floor(0.002 * missionLength + 3.274);
    if (ephemerisIntervalRaw < 4) {
      ephemerisIntervalRaw = 4;
    }
    else if (ephemerisIntervalRaw > 20) {
      ephemerisIntervalRaw = 20;
    }

    const firstEphemerisDate = new Date(missionStartDate);
    firstEphemerisDate.setDate(1);
    const lastEphemerisDate = new Date(missionEndDate);
    lastEphemerisDate.setDate(lastEphemerisDate.getDate() + ephemerisIntervalRaw);
    lastEphemerisDate.setDate(1);
    lastEphemerisDate.setMonth(lastEphemerisDate.getMonth() + 1);

    const firstEphemerisDatestring = firstEphemerisDate.toISOString().substring(0,10);
    const lastEphemerisDatestring = lastEphemerisDate.toISOString().substring(0,10);

    const doStoredEphemeridesMatchDates = (
      localStorage.getItem('firstEphemerisDay') <= firstEphemerisDatestring &&
      localStorage.getItem('lastEphemerisDay') >= lastEphemerisDatestring &&
      localStorage.getItem('ephemerisInterval') === ephemerisIntervalRaw.toString()
    );
    for (let body in sys) {

      if (sys[body].category !== null && !props.mission.included_bodies[sys[body].category]) {
        continue;
      }

      let rawHorizonsData;
      let shouldFetch = false;
      const ephemerisInterval = Math.min((sys[body].maxDx || Infinity), ephemerisIntervalRaw);

      if (!doStoredEphemeridesMatchDates) {
        shouldFetch = true;
      }
      else {
        const storedSys = localStorage.getItem(`sys_${body}`);
        if (!storedSys) {
          shouldFetch = true;
        }
        else {
          const parsedSys = JSON.parse(storedSys);
          if (parsedSys.dx !== ephemerisInterval) {
            shouldFetch = true;
          }
          else {
            sys[body].spln = parsedSys.spln;
            sys[body].dx = parsedSys.dx;
          }
        }
      }

      if (shouldFetch) {
        const result = await xhr('get', '/ephemerides', {
          COMMAND: sys[body].command,
          CENTER: sys[body].parent === 'sol' ? '0' : sys[sys[body].parent].command,
          STEP_SIZE: `${ephemerisInterval}d`,
          START_TIME: firstEphemerisDatestring,
          STOP_TIME: lastEphemerisDatestring,
        });
        await Framework.parseHorizonsData(result.data, body, ephemerisInterval);
        try {
          localStorage.setItem(`sys_${body}`, JSON.stringify({
            spln: sys[body].spln,
            dx: ephemerisInterval,
          }));
        }
        catch(err) {
          console.error(err);
        }
      }
    }
    if (!doStoredEphemeridesMatchDates) {
      localStorage.setItem('firstEphemerisDay', firstEphemerisDatestring);
      localStorage.setItem('lastEphemerisDay', lastEphemerisDatestring);
      localStorage.setItem('ephemerisInterval', ephemerisIntervalRaw);
    }
  }

  function calculateTrajectory() {
    setIsRendering(true);
    setTimeout(async () => {
      try {
        await getHorizonsEphemerides();
        const data = await Framework.calculateTrajectory(props.mission, timeoutCutoff);
        setKinematics(data);
        setPostKinematics(Framework.calculatePostKinematics(props.mission, data));
        setTimeout(() => {
          setIsRendering(false);
        });
      }
      catch(err) {
        setIsRendering(false);
        switch (err.message) {
          case 'invalid_mission_start_date': {
            toast.error('Invalid mission start date');
          }
          break;
          case 'invalid_mission_end_date': {
            toast.error('Invalid mission end date');
          }
          break;
          case 'fuel_exhausted': {
            toast.error('Not enough fuel for burns');
          }
          break;
          default: {
            console.error(err);
            toast.error('Could not calculate trajectory');
          }
        }
      }
    });
  }

  function getAvailableOrigins() {
    const options = [
      ['bar','Barycenter'],
      ['sol','Sol'],
      ['mer','Mercury'],
      ['ven','Venus'],
      ['ter','Earth'],
      ['lun','Luna'],
      ['mar','Mars'],
    ];
    Object.keys(kinematics?.orbits || {}).forEach(key => {
      if (sys[key].parent === 'mar') {
        options.push([key, sys[key].name]);
      }
    });
    options.push(['jup','Jupiter']);
    Object.keys(kinematics?.orbits || {}).forEach(key => {
      if (sys[key].parent === 'jup') {
        options.push([key, sys[key].name]);
      }
    });
    options.push(['sat','Saturn']);
    Object.keys(kinematics?.orbits || {}).forEach(key => {
      if (sys[key].parent === 'sat') {
        options.push([key, sys[key].name]);
      }
    });
    options.push(['ura','Uranus']);
    Object.keys(kinematics?.orbits || {}).forEach(key => {
      if (sys[key].parent === 'ura') {
        options.push([key, sys[key].name]);
      }
    });
    options.push(['nep','Neptune']);
    Object.keys(kinematics?.orbits || {}).forEach(key => {
      if (sys[key].parent === 'nep') {
        options.push([key, sys[key].name]);
      }
    });
    return options;
  }

  function rerender() {
    stopVideo();
    calculateTrajectory();
  }

  if (props.isLoading) {
    return (
      <Spinner is_active/>
    );
  }
  else {
    const autoProps = {
      autoCorrect: 'off',
      autoCapitalize: 'off',
      spellCheck: 'false',
      type: 'text',
    };

    const currentUnixTime = kinematics ? Number(props.mission.start_day) + kinematics.MET[frame] : null;
    const datestring = kinematics ? new Date(currentUnixTime * 1000).toUTCString().substring(5,25) : null;

    return (
      <div className="sandbox">
        <aside className='control-panel left'>
          <div className='control-list'>
            <h3>Mission controls</h3>
            <div className="item-panel control">
              <p className="label">Start date (UTC)</p>
              <input
                value={new Date(props.mission.start_day * 1000).toISOString().substring(0,19)}
                type="datetime-local"
                onChange={(e) => {
                  if (!!e.target.value) {
                    const value = new Date(`${e.target.value}Z`).getTime() / 1000;
                    Utilities.setQueryString(props.router, 'start', value);
                    Utilities.setQueryString(props.router, 'end', props.mission.end_day);
                    props.setMission({start_day: value});
                  }
                }}
              />
              <p className="unit"/>
            </div>
            <div className="item-panel control">
              <p className="label">End date (UTC)</p>
              <input
                value={new Date(props.mission.end_day * 1000).toISOString().substring(0,19)}
                type="datetime-local"
                onChange={(e) => {
                  if (!!e.target.value) {
                    const value = new Date(`${e.target.value}Z`).getTime() / 1000;
                    Utilities.setQueryString(props.router, 'start', props.mission.start_day);
                    Utilities.setQueryString(props.router, 'end', value);
                    props.setMission({end_day: value});
                  }
                }}
              />
              <p className="unit"/>
            </div>
            <div className="item-panel control">
              <p className="label">Frame interval</p>
              <input
                className='input-control'
                type='range'
                min={-7}
                max={2}
                step={1}
                value={Math.log2(86400 / props.mission.frame_interval) * -1}
                onChange = {(event) => {
                  const newValue = 86400 / Math.pow(2, event.target.value * -1);
                  Utilities.setQueryString(props.router, 'frameInterval', newValue);
                  props.setMission({frame_interval: newValue});
                }}
                onMouseUp={(event) => Utilities.setQueryString(props.router, 'frameInterval', 86400 / Math.pow(2, event.target.value * -1))}
              />
              <p className="unit">{(() => {
                switch (props.mission.frame_interval) {
                  case 345600: {
                    return '4 d';
                  }
                  case 172800: {
                    return '2 d';
                  }
                  case 86400: {
                    return '1 d';
                  }
                  case 43200: {
                    return '½ d';
                  }
                  case 21600: {
                    return '6 h';
                  }
                  case 10800: {
                    return '3 h';
                  }
                  case 5400: {
                    return '1½ h';
                  }
                  case 2700: {
                    return '¾ h';
                  }
                  case 1350: {
                    return '22½ m';
                  }
                  case 675: {
                    return '11¼ m';
                  }
                }
              })()}</p>
            </div>
            <div className='dividerHori'></div>
            <h3>Calculator controls</h3>
            <InputNum
              label = {'Calculation timeout'}
              value = {timeoutCutoff}
              units = {'s'}
              onChange = {(value) => setTimeoutCutoff(value)}
            />
            <div className='dividerHori'></div>
            <h3>Included bodies</h3>
            <InputCheck
              label="Planets (besides Earth)"
              value={props.mission.included_bodies.planets}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify());
                props.setMission({included_bodies: {...props.mission.included_bodies, planets: val}});
              }}
            />
            <InputCheck
              label="Martian system"
              value={props.mission.included_bodies.mars}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, mars: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, mars: val}});
              }}
            />
            <InputCheck
              label="Jovian system"
              value={props.mission.included_bodies.jupiter}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, jupiter: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, jupiter: val}});
              }}
            />
            <InputCheck
              label="Saturnian system"
              value={props.mission.included_bodies.saturn}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, saturn: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, saturn: val}});
              }}
            />
            <InputCheck
              label="Uranian system"
              value={props.mission.included_bodies.uranus}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, uranus: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, uranus: val}});
              }}
            />
            <InputCheck
              label="Neptunian system"
              value={props.mission.included_bodies.neptune}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, neptune: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, neptune: val}});
              }}
            />
            <InputCheck
              label="Asteroids"
              value={props.mission.included_bodies.asteroids}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, asteroids: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, asteroids: val}});
              }}
            />
            <InputCheck
              label="Kuiper Belt"
              value={props.mission.included_bodies.kuiper}
              onChange={val => {
                Utilities.setQueryString(props.router, 'includedBodies', JSON.stringify({...props.mission.included_bodies, kuiper: val}));
                props.setMission({included_bodies: {...props.mission.included_bodies, kuiper: val}});
              }}
            />
          </div>
        </aside>
        <OrbitPlot
          mission={props.mission}
          user={props.user}
          size={props.size}
          kinematics={kinematics}
          postKinematics={postKinematics}
          origin={origin}
          scale={scale}
          frame={frame}
          cam={cam}
          setCam={setCam}
          quality={quality}
          futureOrbitRenderMode={futureOrbitRenderMode}
          incrementVideo={incrementVideo}
          playPauseVideo={playPauseVideo}
          stopVideo={stopVideo}
          isPlaying={isPlaying}
          scrubVideo={(value) => setFrame(value)}
          rerender={rerender}
          discardChanges={props.discardChanges}
          saveMission={props.saveMission}
          isDirty={props.isDirty}
          isNew={props.isNew}
          isRendering={isRendering || !kinematics || !postKinematics}
          router={props.router}
        />
        <aside className='control-panel right'>
          <div className='control-list'>
            <h3>Plot controls</h3>
            <InputSel
              label = {'Viewpoint'}
              value = {(() => {
                if (cam.alt === -1.5708 && cam.azi === -1.5708) {
                  return '+x';
                }
                else if (cam.alt === -1.5708 && cam.azi === 1.5708) {
                  return '-x';
                }
                else if (cam.alt === -1.5708 && cam.azi === 3.1416) {
                  return '+y';
                }
                else if (cam.alt === -1.5708 && cam.azi === 0) {
                  return '-y';
                }
                else if (cam.alt === 0 && cam.azi === 0) {
                  return '+z';
                }
                else if (cam.alt === -3.1416 && cam.azi === 0) {
                  return '-z';
                }
                else {
                  return '';
                }
              })()}
              units = {''}
              options = {[
                ['','free'],
                ['+x','+x'],
                ['-x','-x'],
                ['+y','+y'],
                ['-y','-y'],
                ['+z','+z'],
                ['-z','-z'],
              ]}
              onChange = {(value) => {
                let newCam = {
                  alt: null,
                  azi: null,
                };
                if (value === '+x') {
                  newCam.alt = Number((-Math.PI / 2).toFixed(4));
                  newCam.azi = Number((-Math.PI / 2).toFixed(4));
                }
                else if (value === '-x') {
                  newCam.alt = Number((-Math.PI / 2).toFixed(4));
                  newCam.azi = Number((Math.PI / 2).toFixed(4));
                }
                else if (value === '+y') {
                  newCam.alt = Number((-Math.PI / 2).toFixed(4));
                  newCam.azi = Number((Math.PI).toFixed(4));
                }
                else if (value === '-y') {
                  newCam.alt = Number((-Math.PI / 2).toFixed(4));
                  newCam.azi = 0;
                }
                else if (value === '+z') {
                  newCam.alt = 0;
                  newCam.azi = 0;
                }
                else if (value === '-z') {
                  newCam.alt = Number((-Math.PI).toFixed(4));
                  newCam.azi = 0;
                }
                else {
                  newCam = cam;
                }
                setCam(newCam);
                Utilities.setQueryString(props.router, 'camAzi', newCam.azi.toFixed(4));
                Utilities.setQueryString(props.router, 'camAlt', newCam.alt.toFixed(4));
              }}
            />
            <InputSel
              label = {'Origin'}
              value = {origin}
              units = {''}
              options = {availableOrigins}
              onChange = {(value) => {
                Utilities.setQueryString(props.router, 'origin', value);
                setOrigin(value);
              }}
            />
            <div className='item-panel control'>
              <p className='label'>{'Scale'}</p>
              <input
                className='input-control'
                type='range'
                min={scaleRange[0]}
                max={scaleRange[1]}
                step={0.05}
                value={Math.log10(scale)}
                onChange={(event) => setScale(Math.pow(10,Number(event.target.value)))}
                onMouseUp={(event) => Utilities.setQueryString(props.router, 'scale', Math.pow(10,Number(event.target.value)).toFixed(0))}
              />
                <p className='unit'/>
            </div>
            <div className='item-panel control'>
              <p className='label'>{'Playback speed'}</p>
              <input
                className='input-control'
                type='range'
                min={1}
                max={10}
                step={1}
                value={jump}
                onChange={(event) => setJump(parseInt(Number(event.target.value)))}
              />
              <p className='unit'>{jump}x</p>
            </div>
            <div className='item-panel control'>
              <p className='label'>{'Quality'}</p>
              <input
                className='input-control'
                type='range'
                min={qualityRange[0]}
                max={qualityRange[1]}
                step={1}
                value={51 - quality}
                onChange={(event) => setQuality(51 - parseInt(Number(event.target.value)))}
                onMouseUp={(event) => Utilities.setQueryString(props.router, 'quality', parseInt(Number(event.target.value)))}
              />
            </div>
            <InputSel
              label = {'Future orbit rendering'}
              value = {futureOrbitRenderMode}
              units = {''}
              options = {[
                ['all', 'All'],
                ['one', 'One max'],
              ]}
              onChange = {(value) => {
                Utilities.setQueryString(props.router, 'futureOrbitRenderMode', value);
                setFutureOrbitRenderMode(value);
              }}
            />
            <div className='dividerHori'></div>
            <h3>Instantaneous data</h3>
            <OutputText
              label = {'Frame'}
              value = {frame}
              units = {''}
            />
            <OutputText
              label = {'Datetime (UTC)'}
              value = {datestring}
              units = {''}
            />
            <div className='dividerHori'></div>
            <h3>Calculator metadata</h3>
            <OutputText
              label = {'Frame count'}
              value = {kinematics?.MET.length}
            />
            <OutputText
              label = {'Calculation time'}
              value = {kinematics?.calcTime.toFixed(1)}
              units = {'ms'}
            />
            <OutputText
              label = {'Did abort calculation?'}
              value = {kinematics?.didAbort.toString()}
              units = {''}
            />
          </div>
        </aside>
      </div>
    );
  }

}
