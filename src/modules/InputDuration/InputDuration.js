export default function InputDuration(props) {

  const years = Math.floor(props.value / (60 * 60 * 24 * 365));
  const days = Math.floor((props.value % (60 * 60 * 24 * 365)) / (60 * 60 * 24));
  const hours = Math.floor((props.value % (60 * 60 * 24)) / (60 * 60));

  return (
    <div className='item-panel control'>
      <p className='label'>{props.label}</p>
      <div className="input-duration">
        <input
          className="duration-years"
          onChange = {(e) => {
            const newYears = Number(e.target.value);
            props.onChange(newYears*(60*60*24*365) + days*(60*60*24) + hours*(60*60));
          }}
          value = {years}
          type = 'number'
          min="0"
        />
        <p className="unit">y</p>
        <input
          className="duration-days"
          onChange = {(e) => {
            const newDays = Number(e.target.value);
            props.onChange(years*(60*60*24*365) + newDays*(60*60*24) + hours*(60*60));
          }}
          value = {days}
          type = 'number'
          min="0"
        />
        <p className="unit">d</p>
        <input
          className="duration-hours"
          onChange = {(e) => {
            const newHours = Number(e.target.value);
            props.onChange(years*(60*60*24*365) + days*(60*60*24) + newHours*(60*60));
          }}
          value = {hours}
          type = 'number'
          min="0"
        />
        <p className="unit">h</p>
      </div>
    </div>
  );

}
