export default class InputText extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='item-panel control item-title'>
        <p className='label'>{this.props.label}</p>
        <input
          className = 'input-control'
          onChange = {(text) => this.props.onChange(text.target.value)}
          value = {this.props.value}
          type = 'text'
          autoCorrect = 'off'
          autoCapitalize = 'off'
          spellCheck = 'false'
        />
      </div>
    );
  }
  
};
