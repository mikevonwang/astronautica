export default function LegalFooter(props) {

  return (
    <footer>
      <ul>
        <li><a href={'mailto:team@astronautica.io'}>{'Contact'}</a></li>
        <li><A href={'/about'}>{'About'}</A></li>
      </ul>
      <p>{'© ' + (new Date().getFullYear()) + ' '}</p>
    </footer>
  );

}
