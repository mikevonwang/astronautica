export default class OutputText extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='item-panel report'>
        <p className='label'>{this.props.label}</p>
        <p className='value'>{this.props.value}</p>
        <p className='unit'>{this.props.units}</p>
      </div>
    );
  }

};
