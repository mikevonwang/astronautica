export default class Spinner extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      is_spinning: this.props.is_active,
    };
    this.is_unmounted = false;
  }

  static getDerivedStateFromProps(props, state) {
    if (props.is_active === true && state.is_spinning === false) {
      return ({
        is_spinning: true,
      });
    }
    else {
      return null;
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.is_active === true && this.props.is_active === false) {
      setTimeout(() => {
        if (this.is_unmounted === false) {
          this.setState({
            is_spinning: false,
          });
        }
      }, 300);
    }
  }

  componentWillUnmount() {
    this.is_unmounted = true;
  }

  render() {
    const class_spinner_visible = (this.props.is_active) ? ' visible' : '';
    const class_spinner_spinning = (this.state.is_spinning) ? ' spinning' : '';
    const class_spinner_white = (this.props.is_white) ? ' white' : '';

    let class_size = '';
    switch (this.props.size) {
      case 'mini':
        class_size = ' mini';
      break;
      case 'micro':
        class_size = ' micro';
      break;
    }

    const style_ring = (this.props.color) ? {borderColor: this.props.color} : {};

    return (
      <div className={'spinner' + class_spinner_visible + class_spinner_spinning + class_size + class_spinner_white} key='spinner'>
        <div className='level'>
          <div className='window'>
            <div className='ring' style={style_ring}></div>
          </div>
        </div>
        <div className='level'>
          <div className='window'>
            <div className='ring' style={style_ring}></div>
          </div>
        </div>
      </div>
    );
  }

}
