export default function ModalConfirm(props) {

  function onClick(callback) {
    props.setModal(null);
    if (callback) {
      callback();
    }
  }

  let buttons = props.modal.buttons.map((button,i) => {
    return (
      <button className={'btn big ' + button.class} onClick={() => onClick(button.action)} key={'modal-button-' + i}>{button.label}</button>
    );
  });

  return (
    <div className='modal-panel confirm'>
      <header className='modal-panel'>
        <h3>{props.modal.title}</h3>
      </header>
      <div className='modal-content'>
        {props.modal.body}
      </div>
      <footer className='modal-panel'>
        <button className='btn big' onClick={onClick}>{'Cancel'}</button>
        {buttons}
      </footer>
    </div>
  );

}
