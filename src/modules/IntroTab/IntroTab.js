export default function IntroTab(props) {

  const [isPeeking, setIsPeeking] = React.useState(false);
  const [isVisible, setIsVisible] = React.useState(false);

  const isUnmounted = React.useRef(false);

  React.useEffect(() => {
    setTimeout(() => {
      if (isUnmounted.current === false) {
        setIsPeeking(true);
      }
    }, 2000);
    return () => {
      isUnmounted.current = true;
    }
  }, []);

  const classIsPeeking = isPeeking ? 'is-peeking' : '';
  const classIsVisible = isVisible ? 'is-visible' : '';

  return (
    <div className={`intro-tab ${classIsPeeking} ${classIsVisible}`}>
      <div className="tab-wrapper">
        <div className="tabbling">
          <span>Hi! Are you new here?</span>
          <button className="btn white" onClick={() => setIsVisible(true)}>Yes, help!</button>
          <button className="btn white" onClick={() => setIsPeeking(false)}>Nah</button>
        </div>
        <div className="tab-body">
          <div className="tab-column tab-column-logo">
            <div className="logo-pedastal">
              <img src="/img/logo.png"/>
            </div>
          </div>
          <div className="tab-column">
            <strong>This is Astronautica. It:</strong>
            <ul>
              <li>plots spacecraft trajectories</li>
              <li>animates orbital maneuvers</li>
              <li>is fully numeric</li>
            </ul>
          </div>
          <div className="tab-column">
            <strong>Start with an example mission:</strong>
            <div className="example-list">
              <A href="/example/lunar-flyby?origin=ter&scale=2e5&periapsisTarget=lun" className="btn white">Lunar flyby</A>
              <A href="/example/mars-flyby?periapsisTarget=mar" className="btn white">Mars flyby</A>
              <A href="/example/venus-flyby?periapsisTarget=ven" className="btn white">Venus flyby</A>
              <A href="/example/jupiter-flyby?origin=jup&scale=8e5&periapsisTarget=jup" className="btn white">Jupiter flyby</A>
              <A href="/example/high-earth-orbit?origin=ter&scale=2e4&interval=100" className="btn white">High earth orbit</A>
            </div>
          </div>
          <div className="tab-column">
            <strong>Useful links:</strong>
            <ul>
              <li><A href="/about">About Astronautica</A></li>
              <li><a href="https://gitlab.com/mikevonwang/astronautica">Documentation</a></li>
              <li><a href="https://gitlab.com/mikevonwang/astronautica/-/issues">Bug tracker</a></li>
            </ul>
          </div>
          <button onClick={() => {setIsVisible(false); setIsPeeking(false)}} className="btn clear close-intro-tab">×</button>
        </div>
      </div>
    </div>
  );

}
