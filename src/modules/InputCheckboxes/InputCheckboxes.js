export default class InputCheckboxes extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  setValue(index) {
    this.props.onChange(this.props.options.map((option,i) => {
      if (i === index) {
        return Object.assign({}, option, {
          value: !option.value,
        });
      }
      else {
        return Object.assign({}, option);
      }
    }));
  }

  render() {

    const class_horizontal = (this.props.horizontal === true) ? ' horizontal' : '';

    const options = this.props.options.map((option,i) => {
      const class_name = (option.value) ? ' checked' : ' unchecked';
      return (
        <li onClick={(e) => {e.preventDefault(); this.setValue(i)}} key={'option-' + i}>
          <label>{option.label}</label>
          <button className={'checkbox' + class_name} type='button'></button>
          <input type='checkbox' checked={option.value} readOnly></input>
        </li>
      );
    });

    return (
      <div className={'input-checkboxes ' + (this.props.className || '') + class_horizontal}>
        <ul>
          {options}
        </ul>
      </div>
    );
  }

}
