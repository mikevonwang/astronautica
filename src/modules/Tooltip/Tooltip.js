export default class Tooltip extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      is_visible: false,
    };
    this.unmounted = false;
  }

  componentDidMount() {
    if (this.props.is_visible === true) {
      setTimeout(() => {
        if (this.unmounted === false) {
          this.setState({
            is_visible: true,
          });
        }
      }, 100);
    }
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.is_visible === false && this.state.is_visible === true && this.props.pause < Infinity) {
      setTimeout(() => {
        if (this.unmounted === false) {
          this.setState({
            is_visible: false,
          });
        }
      }, this.props.pause);
    }
    else if (prevProps.is_visible === false && this.props.is_visible === true) {
      this.setState({
        is_visible: true,
      });
    }
    else if (prevProps.is_visible === true && this.props.is_visible === false) {
      this.setState({
        is_visible: false,
      });
    }
  }

  render() {
    const class_visibility = (this.state.is_visible) ? ' visible' : '';
    const class_error = (this.props.type === 'error') ? ' error' : '';

    const text = (Array.isArray(this.props.text) ? this.props.text : [this.props.text]).map((line,i) => {
      return (
        <span key={i}>{line}<br/></span>
      );
    });

    const style_box = {};
    const style_arrow = {};
    switch (this.props.position) {
      case 'top':
      case 'bottom':
        style_box.left = 'calc(50% + ' + this.props.offset + 'px)';
        style_arrow.left = 'calc(50% + ' + (-this.props.offset) + 'px)';
      break;
      case 'right':
      case 'left':
        style_box.top = 'calc(50% + ' + this.props.offset + 'px)';
        style_arrow.top = 'calc(50% + ' + (-this.props.offset) + 'px)';
      break;
    }

    return (
      <div className={'tooltip position-' + this.props.position + ' ' + this.props.className + class_error + class_visibility} style={style_box}>
        <p>{text}</p>
        {this.props.children}
        <div className="tooltip-arrow" style={style_arrow}/>
      </div>
    );
  }

}

Tooltip.defaultProps = {
  pause: 2500,
  position: 'top',
  offset: 0,
  className: '',
};
