import FormInputText from '../FormInputText/FormInputText';

export default class FormInputPassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      text_visible: false,
    };
  }

  onSwitchMode(e) {
    e.preventDefault();
    this.setState({
      text_visible: !this.state.text_visible,
    });
  }

  render() {
    const text_button = (this.state.text_visible ? 'hide' : 'show');

    return (
      <div className='inputpassword'>
        <FormInputText
          type = {this.state.text_visible ? 'text' : 'password'}
          {...this.props}
        />
        <button className='btn white micro' onClick={this.onSwitchMode.bind(this)} type='button'>{text_button}</button>
      </div>
    );
  }

}
