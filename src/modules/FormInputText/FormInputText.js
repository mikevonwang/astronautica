export default function FormInputText(props) {

  return (
    <input
      id = {props.id}
      className = {props.className}
      type = {props.type}
      value = {props.value}
      onChange = {(text) => props.onChange(text.target.value)}
      onKeyDown = {(props.onKeyDown) ? (e) => props.onKeyDown(e) : null}
      autoCorrect = 'off'
      autoCapitalize = 'off'
      autoComplete = 'username'
      spellCheck = 'false'
      disabled = {props.disabled}
    />
  );

}
