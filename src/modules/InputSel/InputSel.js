export default class InputSel extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    var options = [];
    for (var i = 0; i<this.props.options.length; i++) {
      options.push(
        <option value={this.props.options[i][0]} key={this.props.label + '_' + this.props.options[i][0]}>{this.props.options[i][1]}</option>
      );
    }
    return (
      <div className='item-panel control'>
        <p className='label'>{this.props.label}</p>
        <select className='input-control' onChange = {(event) => this.props.onChange(event.target.value)} value = {this.props.value}>
          {options}
        </select>
        <p className='unit'>{this.props.units}</p>
      </div>
    );
  }

};
