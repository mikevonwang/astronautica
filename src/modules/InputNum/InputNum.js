export default class InputNum extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='item-panel control'>
        <p className='label'>{this.props.label}</p>
        <input
          className = 'input-control'
          onChange = {(text) => this.props.onChange(Number(text.target.value))}
          value = {this.props.value}
          type = 'text'
          autoCorrect = 'off'
          autoCapitalize = 'off'
          spellCheck = 'false'
        />
        <p className='unit'>{this.props.units}</p>
      </div>
    );
  }

};
