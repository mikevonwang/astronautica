import * as Framework from '../../core/Framework';
import * as Utilities from '../../global/js/utilities';

import Spinner from '../Spinner/Spinner';

const Colors = {
  sol: {body: '#fcf4a3', grid: '#eedc82'},
  mer: {body: '#888888', grid: '#666666'},
  ven: {body: '#f5cf85', grid: '#d2ad64'},
  ter: {body: '#0080ff', grid: '#0a5adf'},
  lun: {body: '#aaaaaa', grid: '#888888'},
  mar: {body: '#ff6666', grid: '#dd444c'},
  pho: {body: '#aaaaaa', grid: '#888888'},
  dei: {body: '#aaaaaa', grid: '#888888'},
  cer: {body: '#aaaaaa', grid: '#888888'},
  jup: {body: '#f1ab72', grid: '#d29563'},
  iox: {body: '#f2e273', grid: '#bdac31'},
  eur: {body: '#c4c3c1', grid: '#b2816b'},
  gan: {body: '#968c79', grid: '#645f5a'},
  cal: {body: '#98867b', grid: '#4f3f2f'},
  sat: {body: '#cebe84', grid: '#baa452'},
  enc: {body: '#d2d0d3', grid: '#9fb7be'},
  thy: {body: '#aaaaaa', grid: '#888888'},
  dio: {body: '#aaaaaa', grid: '#888888'},
  rhe: {body: '#aaaaaa', grid: '#888888'},
  ttn: {body: '#d6c05e', grid: '#bb8e47'},
  iap: {body: '#aaaaaa', grid: '#888888'},
  ura: {body: '#add1d6', grid: '#74adb5'},
  ari: {body: '#aaaaaa', grid: '#888888'},
  umb: {body: '#aaaaaa', grid: '#888888'},
  tta: {body: '#aaaaaa', grid: '#888888'},
  obe: {body: '#aaaaaa', grid: '#888888'},
  mir: {body: '#aaaaaa', grid: '#888888'},
  nep: {body: '#6997d8', grid: '#395bbb'},
  tri: {body: '#aea19d', grid: '#373737'},
  plu: {body: '#d2ad90', grid: '#af8e6f'},
  cha: {body: '#aaaaaa', grid: '#888888'},
  eri: {body: '#aaaaaa', grid: '#888888'},
  hau: {body: '#aaaaaa', grid: '#888888'},
  mak: {body: '#aaaaaa', grid: '#888888'},
  gng: {body: '#aaaaaa', grid: '#888888'},
  qua: {body: '#aaaaaa', grid: '#888888'},
  sed: {body: '#aaaaaa', grid: '#888888'},
  slv: [
    '#42c8ff',
    '#f75972',
    '#9bde9e',
    '#edac57',
    '#6c4ca5',
  ],
};

export default function OrbitPlot(props) {

  const [camRef, setCamRef] = React.useState([null, null]);
  const [camIns, setCamIns] = React.useState([0,0]);
  const [mseRef, setMseRef] = React.useState([null, null]);
  const [camMoving, setCamMoving] = React.useState(false);

  React.useEffect(() => {
    if (!camMoving) {
      setCamera();
    }
  }, [props.cam.alt, props.cam.azi]);

  function setCamera() {
    const newCamRef = [
      ( props.cam.azi * props.size) / (2 * Math.PI),
      (-props.cam.alt * props.size) / (2 * Math.PI),
    ];
    setCamRef(newCamRef);
    setCamIns(newCamRef);
    setMseRef([null, null]);
  }

  function getKinematicsPaths() {

    const plotSkip = props.quality;

    let orbits = {};
    let orbitsHidden = {};
    for (let body in props.kinematics.orbits) {
      let hidden = false;
      if (!props.kinematics.orbits[body]) {
        continue;
      }
      if (body === props.origin) {
        continue;
      }
      if ((
        (sys[body].parent === props.origin) ||
        (props.origin === 'bar' && sys[body].parent === 'sol') ||
        (props.origin !== 'bar' && sys[body].parent === sys[props.origin].parent) ||
        (props.origin !== 'bar' && body === sys[props.origin].parent)
      ) === false) {
        continue;
      }
      if (sys[body].parent !== 'sol' && !!sys[body].maxScale && props.scale > sys[body].maxScale) {
        continue;
      }
      orbits[body] = '';
      orbitsHidden[body] = '';
      let tmly_1 = 0;
      let wind = 0;
      for (let i=props.frame; i<props.kinematics.MET.length; i+=plotSkip) {
        if (props.futureOrbitRenderMode === 'one') {
          const tmly = getTmly(body, props.origin, i);
          if (tmly_1 !== 0) {
            if (tmly_1 - tmly > 4) {
              wind += tmly - (tmly_1 - (2 * Math.PI));
            }
            else if (tmly - tmly_1 > 4) {
              wind += tmly - (tmly_1 + (2 * Math.PI));
            }
            else {
              wind += tmly - tmly_1;
            }
          }
          if (wind > 2 * Math.PI) {
            break;
          }
          tmly_1 = tmly;
        }

        const svgCoord = toSVG(props.kinematics.orbits[body].pos[i],i,1);
        if (svgCoord.z < 0) {
          if (hidden) {
            orbitsHidden[body] += svgPrefix(i-props.frame, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
          }
          else {
            orbitsHidden[body] += svgPrefix(i-props.frame, true) + svgCoord.x + ' ' + svgCoord.y + ' ';
            orbits[body] += svgPrefix(i-props.frame, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
          }
          hidden = true;
        }
        else {
          if (hidden) {
            orbits[body] += svgPrefix(i-props.frame, true) + svgCoord.x + ' ' + svgCoord.y + ' ';
            orbitsHidden[body] += svgPrefix(i-props.frame, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
          }
          else {
            orbits[body] += svgPrefix(i-props.frame, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
          }
          hidden = false;
        }
      }
      if (props.origin === 'bar') {
        for (let i=0; i<props.postKinematics.fullOrbits[body].length; i++) {
          const svgCoord = toSVG2(props.postKinematics.fullOrbits[body][i],i,1);
          if (svgCoord.z < 0) {
            if (hidden) {
              orbitsHidden[body] += svgPrefix(1, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
            }
            else {
              orbitsHidden[body] += svgPrefix(1, true) + svgCoord.x + ' ' + svgCoord.y + ' ';
              orbits[body] += svgPrefix(1, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
            }
            hidden = true;
          }
          else {
            if (hidden) {
              orbits[body] += svgPrefix(1, true) + svgCoord.x + ' ' + svgCoord.y + ' ';
              orbitsHidden[body] += svgPrefix(1, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
            }
            else {
              orbits[body] += svgPrefix(1, false) + svgCoord.x + ' ' + svgCoord.y + ' ';
            }
            hidden = false;
          }
        }
      }
    }

    let wireframes = {};
    let wireframesHidden = {};
    for (let body in props.kinematics.orbits) {
      if ((
        (body === 'sol' && props.scale <= 1e8) ||
        (isGasGiant(body) && props.scale <= 1e6) ||
        (!isGasGiant(body) && props.scale <= 5e5)
      ) && (
        (body === props.origin) ||
        (props.origin === 'bar' && body === 'sol') ||
        (props.origin !== 'bar' && body === sys[props.origin].parent && sys[props.origin].parent !== 'sol')
      )) {
        let wireframe = '';
        let wireframeHidden = '';
        const rOrigin = (props.origin === 'bar') ? [
          props.kinematics.orbits[body].pos[props.frame][0],
          props.kinematics.orbits[body].pos[props.frame][1],
          props.kinematics.orbits[body].pos[props.frame][2],
        ] : [
          props.kinematics.orbits[body].pos[props.frame][0] - props.kinematics.orbits[props.origin].pos[props.frame][0],
          props.kinematics.orbits[body].pos[props.frame][1] - props.kinematics.orbits[props.origin].pos[props.frame][1],
          props.kinematics.orbits[body].pos[props.frame][2] - props.kinematics.orbits[props.origin].pos[props.frame][2],
        ];
        let rOriginRotated = rotateToCameraView(rOrigin);
        ['lat','lon'].forEach(x => {
          for (let i=0; i<sys[body].grid[x].length; i++) {
            let hidden = false;
            for (let j=0; j<sys[body].grid[x][i].length; j++) {
              const rotatedPoint = rotateToCameraView([
                sys[body].grid[x][i][j][0] + rOrigin[0],
                sys[body].grid[x][i][j][1] + rOrigin[1],
                sys[body].grid[x][i][j][2] + rOrigin[2],
              ]);
              const svgCoord = {
                x: km2px(rotatedPoint[0], 1),
                y: km2px(rotatedPoint[1], 1),
                z: km2px(rotatedPoint[2], 1),
              }
              if (rotatedPoint[2] < rOriginRotated[2]) {
                wireframeHidden += svgPrefix(j,hidden !== true) + svgCoord.x + ' ' + svgCoord.y + ' ';
                hidden = true;
              }
              else {
                wireframe += svgPrefix(j,hidden !== false) + svgCoord.x + ' ' + svgCoord.y + ' ';
                hidden = false;
              }
            }
          }
        });
        wireframes[body] = wireframe;
        wireframesHidden[body] = wireframeHidden;
      }
    }

    return({
      orbits,
      orbitsHidden,
      wireframes,
      wireframesHidden,
    });

  }

  function isGasGiant(body) {
    return (['jup','sat','ura','nep']).includes(body);
  }

  function getTmly(body, parentBody, i) {
    let tmly;
    if (parentBody === 'bar') {
      tmly = Math.atan2(
        props.kinematics.orbits[body].pos[i][1],
        props.kinematics.orbits[body].pos[i][0]
      );
    }
    else {
      tmly = Math.atan2(
        props.kinematics.orbits[body].pos[i][1] - props.kinematics.orbits[parentBody].pos[i][1],
        props.kinematics.orbits[body].pos[i][0] - props.kinematics.orbits[parentBody].pos[i][0]
      );
    }
    if (tmly < 0) {
      tmly += (2 * Math.PI);
    }
    return tmly;
  }

  function toSVG2(y0,i,mode) { // convert from kilometers to SVG coordinates
    let y1 = [];
    let y2 = [];

    let origin;
    if (i === -1 || props.origin === 'bar') {
      origin = [0, 0, 0];
    }
    else {
      origin = props.postKinematics.fullOrbits[props.origin][i];
    }
    for (let a=0; a<3; a++) {
      y1[a] = y0[a] - origin[a];
    }

    y2 = rotateToCameraView(y1);

    return ({
      x: km2px(y2[0],mode),
      y: km2px(y2[1],mode),
      z: km2px(y2[2],0),
    });
  }

  function toSVG(y0,t,mode) { // convert from kilometers to SVG coordinates
    let y1 = [];
    let y2 = [];

    let origin;
    if (t === -1 || props.origin === 'bar') {
      origin = [0, 0, 0];
    }
    else {
      origin = props.kinematics.orbits[props.origin].pos[t];
    }
    for (let a=0; a<3; a++) {
      y1[a] = y0[a] - origin[a];
    }

    y2 = rotateToCameraView(y1);

    return ({
      x: km2px(y2[0],mode),
      y: km2px(y2[1],mode),
      z: km2px(y2[2],0),
    });
  }

  function rotateToCameraView(y1) {
    let y2 = [];
    let y3 = [];

    y2[0] = Math.cos(props.cam.azi) * y1[0] - Math.sin(props.cam.azi) * y1[1];
    y2[1] = Math.sin(props.cam.azi) * y1[0] + Math.cos(props.cam.azi) * y1[1];
    y2[2] = y1[2]

    y3[0] = y2[0];
    y3[1] = Math.cos(props.cam.alt) * y2[1] - Math.sin(props.cam.alt) * y2[2];
    y3[2] = Math.sin(props.cam.alt) * y2[1] + Math.cos(props.cam.alt) * y2[2];

    y3[1] *= -1; // svg ordinate convention is opposite that of physics

    return y3;
  }

  function km2px(km,mode) {
    let origin;
    if (mode === 0) { // km is a distance
      origin = 0;
    }
    else if (mode === 1) { // km is a coordinate
      origin = (props.size/2);
    }
    return (km * ((props.size / 2) / (3 * Number(props.scale))) + origin);
  }

  function svgPrefix(i,zSwitch) {
    if (i === 0 || zSwitch === true) {
      return ('M');
    }
    else {
      return ('L');
    }
  }

  function onMouseDown(e) {
    setCamMoving(true);
    setCamRef(camIns);
    setMseRef([e.clientX,e.clientY]);
  }

  function onMouseUp() {
    if (camMoving) {
      setCamMoving(false);
      setMseRef([null, null]);
      Utilities.setQueryString(props.router, 'camAzi', props.cam.azi.toFixed(4));
      Utilities.setQueryString(props.router, 'camAlt', props.cam.alt.toFixed(4));
    }
  }

  function dragSVG(x, y) {
    if (camMoving === true) {
      let newCamIns = [camRef[0] + x - mseRef[0], camRef[1] - y + mseRef[1]];
      const camLimit = (props.size / 2);
      if (newCamIns[0] < -camLimit) {
        newCamIns[0] = -camLimit;
      }
      else if (newCamIns[0] > camLimit) {
        newCamIns[0] = camLimit;
      }
      if (newCamIns[1] < 0) {
        newCamIns[1] = 0;
      }
      else if (newCamIns[1] > camLimit) {
        newCamIns[1] = camLimit;
      }
      props.setCam({
        azi:  ((newCamIns[0] / (props.size / 4)) * Math.PI / 2),
        alt: -((newCamIns[1] / (props.size / 4)) * Math.PI / 2),
      });
      setCamIns(newCamIns);
    }
  }

  function getGridPaths() {

    const axisX = 'M0 ' + (props.size/2) +  ' L' + props.size + ' ' + (props.size/2);
    const axisY = 'M' + (props.size/2) + ' 0 L' + (props.size/2) + ' ' + props.size;

    const scale = Number(props.scale);
    const max = scale * 3;
    let exponent = Math.floor(Math.log10(scale));
    let base = Math.pow(10, exponent);
    if (max / base < 5) {
      base /= 2;
      exponent -= 1;
    }
    else if (max / base > 10) {
      base *= 2;
    }
    let gridX = '';
    let gridY = '';
    for (let x=base; x<=max; x+=base) {
      gridX += 'M0 ' + km2px(x,1) + ' L' + props.size + ' ' + km2px(x,1) + ' ';
      gridX += 'M0 ' + km2px(-x,1) + ' L' + props.size + ' ' + km2px(-x,1) + ' ';
      gridY += 'M' + km2px(x,1) + ' 0 L' + km2px(x,1) + ' ' + props.size + ' ';
      gridY += 'M' + km2px(-x,1) + ' 0 L' + km2px(-x,1) + ' ' + props.size + ' ';
    }

    let label = '';
    if (base.toString()[0] === '1') {
      label += '10';
    }
    else {
      label += `${base.toString()[0]}*10`;
    }

    let dimensionLine = '';
    dimensionLine += `M${props.size/2} ${props.size-14} L${km2px(base,1)} ${props.size-14}`;
    dimensionLine += `M${props.size/2} ${props.size-17} L${props.size/2} ${props.size-11}`;
    dimensionLine += `M${km2px(base,1)} ${props.size-17} L${km2px(base,1)} ${props.size-11}`;

    return (
      <>
        <path d={gridX} stroke={'#333333'} strokeWidth={1} fill={'none'}/>
        <path d={gridY} stroke={'#333333'} strokeWidth={1} fill={'none'}/>
        <path d={axisX} stroke={'#444444'} strokeWidth={1} fill={'none'}/>
        <path d={axisY} stroke={'#444444'} strokeWidth={1} fill={'none'}/>
        <text x={km2px(base,1)+6} y={props.size-10} fill="#999">{label}^{exponent} km</text>
        <path d={dimensionLine} stroke="#999" fill="none"/>
      </>
    );
  }

  function getGimbalPaths() {
    const origin = [40, props.size-40, 0];
    const axes = [{
      label: 'x',
      point: [30, 0, 0],
      color: '#e43636',
    },{
      label: 'y',
      point: [0, 30, 0],
      color: '#27a23b',
    },{
      label: 'z',
      point: [0, 0, 30],
      color: '#007bd8',
    }];

    const hydratedAxes = axes.map(axis => {
      const rotatedPoint = rotateToCameraView(axis.point);
      let path = '';
      path += 'M' + origin[0] + ' ' + origin[1] + ' ';
      path += 'l' + rotatedPoint[0] + ' ' + rotatedPoint[1] + ' ';
      return {
        ...axis,
        rotatedPoint,
        component: (
          <React.Fragment key={axis.label}>
            <path d={path} stroke={axis.color} fill="none" strokeDasharray={rotatedPoint[2] < 0 ? '5,5' : undefined} opacity={rotatedPoint[2] < 0 ? 0.7 : undefined}/>
            <text x={origin[0] + rotatedPoint[0]} y={origin[1] + rotatedPoint[1]} fill={axis.color}>{axis.label}</text>
          </React.Fragment>
        ),
      };
    });

    const sortedHydratedAxes = Utilities.sort(hydratedAxes, [axis => axis.rotatedPoint[2]]);

    return sortedHydratedAxes.map(axis => axis.component);
  }

  let missionPlot;
  if (!props.isRendering) {
    const kinePaths = getKinematicsPaths();

    let orbits = [];
    let orbitsHidden = [];
    let bodies = [];
    let bodiesHidden = [];
    let SOIs = [];
    let SOIsHidden = [];
    let names = [];
    let namesHidden = [];
    let originBody;
    let originName;
    const minNameDistance = 14;
    for (let body in props.kinematics.orbits) {
      if (body === props.origin || !!kinePaths.orbits[body]) {
        let svgCoord = toSVG(props.kinematics.orbits[body].pos[props.frame],props.frame,1);
        let r = km2px(sys[body].rad,0);
        orbits.push(
          <path d={kinePaths.orbits[body]} stroke={Colors[body].body} strokeWidth={1} fill={'none'} key={'path_' + body}/>
        );
        orbitsHidden.push(
          <path d={kinePaths.orbitsHidden[body]} stroke={Colors[body].body} strokeWidth={1} fill={'none'} key={'path_' + body} opacity={0.7}/>
        );
        if (svgCoord.z < 0) {
          bodiesHidden.push(
            <circle cx={svgCoord.x} cy={svgCoord.y} r={r} strokeWidth={0} fill={Colors[body].body} key={'body_' + body}/>
          );
          if (r < 6) {
            SOIsHidden.push(
              <circle cx={svgCoord.x} cy={svgCoord.y} r={6} strokeWidth={0} fill={Colors[body].body} opacity={0.5} key={'SOI_' + body}/>
            );
          }
          if (!sys[body].maxScale || props.scale < sys[body].maxScale) {
            namesHidden.push(
              <text x={svgCoord.x} y={svgCoord.y + Math.max(r+10, minNameDistance)} fill={Colors[body].body} textAnchor="middle" key={'name_' + body}>{sys[body].name}</text>
            );
          }
        }
        else {
          if (body !== props.origin) {
            bodies.push(
              <circle cx={svgCoord.x} cy={svgCoord.y} r={r} strokeWidth={0} fill={Colors[body].body} key={'body_' + body}/>
            );
            if (!sys[body].maxScale || props.scale < sys[body].maxScale) {
              names.push(
                <text x={svgCoord.x} y={svgCoord.y + Math.max(r+10, minNameDistance)} fill={Colors[body].body} textAnchor="middle" key={'name_' + body}>{sys[body].name}</text>
              );
            }
          }
          else {
            originBody = (
              <circle cx={svgCoord.x} cy={svgCoord.y} r={r} strokeWidth={0} fill={Colors[body].body} key={'body_' + body}/>
            );
            originName = (
              <text x={svgCoord.x} y={svgCoord.y + Math.max(r+10, minNameDistance)} fill={Colors[body].body} textAnchor="middle" key={'name_' + body}>{sys[body].name}</text>
            );
          }
          if (r < 6) {
            SOIs.push(
              <circle cx={svgCoord.x} cy={svgCoord.y} r={6} strokeWidth={0} fill={Colors[body].body} opacity={0.5} key={'SOI_' + body}/>
            );
          }
        }
      }
    }

    let wireframes = [];
    let originWireframes = [];
    for (let body in kinePaths.wireframes) {
      if (body !== props.origin) {
        wireframes.push(
          <path d={kinePaths.wireframesHidden[body]} stroke={Colors[body].grid} strokeWidth={1} fill={'none'} opacity={0.2} key={'wireframe_hidden_' + body}/>
        );
        wireframes.push(
          <path d={kinePaths.wireframes[body]} stroke={Colors[body].grid} strokeWidth={1} fill={'none'} key={'wireframe_' + body}/>
        );
      }
      else {
        originWireframes.push(
          <path d={kinePaths.wireframesHidden[body]} stroke={Colors[body].grid} strokeWidth={1} fill={'none'} opacity={0.2} key={'wireframe_hidden_' + body}/>
        );
        originWireframes.push(
          <path d={kinePaths.wireframes[body]} stroke={Colors[body].grid} strokeWidth={1} fill={'none'} key={'wireframe_' + body}/>
        );
      }
    }

    missionPlot = (
      <>
        {bodiesHidden}
        {namesHidden}
        {orbitsHidden}
        {SOIsHidden}
        {originBody}
        {originName}
        {originWireframes}
        {bodies}
        {wireframes}
        {names}
        {orbits}
        {SOIs}
      </>
    );
  }

  return (
    <div className='plotbox'>
      <svg
        className='plot'
        width={props.size}
        height={props.size}
        draggable='true'
        onMouseDown={(e) => onMouseDown(e)}
        onMouseUp={() => onMouseUp()}
        onMouseLeave={() => onMouseUp()}
        onMouseMove={(e) => dragSVG(e.clientX,e.clientY)}
      >
        <rect x={0} y={0} width={props.size} height={props.size} strokeWidth={0} fill={'#222222'}/>
        {getGridPaths()}
        {getGimbalPaths()}
        {missionPlot}
      </svg>
      <div className='playback'>
        <button className='btn input-playback blue' onClick ={props.rerender} title={'Rerender mission'}>&#x21bb;</button>
        <input
          className = 'input-playback'
          type='range'
          min={0}
          max={props.kinematics ? props.kinematics.MET.length - 1 : 1}
          step={1}
          value={props.frame}
          onChange={(event) => props.scrubVideo(Number(event.target.value))}
        />
        <button className='btn input-playback' onClick={() => props.incrementVideo(-1)} title ={'Go back one frame'}>{'<<'}</button>
        <button className='btn input-playback' onClick={props.playPauseVideo}          title ={'Play/pause'}>{props.isPlaying ? '-' : '>'}</button>
        <button className='btn input-playback' onClick={props.stopVideo}               title ={'Go to first frame'}>{'×'}</button>
        <button className='btn input-playback' onClick={() => props.incrementVideo(1)}  title ={'Go forward one frame'}>{'>>'}</button>
      </div>
      <div className={`spinner-wrapper ${props.isRendering ? 'is-rendering' : ''}`}>
        <Spinner is_active={props.isRendering} is_white/>
      </div>
    </div>
  );

}
