export default class InputCheck extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='item-panel control'>
        <p className='label'>{this.props.label}</p>
        <input
          className = 'input-control'
          onChange = {(event) => this.props.onChange(!this.props.value)}
          value = {this.props.value}
          checked = {this.props.value}
          type = 'checkbox'
        />
      </div>
    );
  }

};
