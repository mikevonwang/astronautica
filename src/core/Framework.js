let thrust = [];
const tau = 2*Math.PI;
const deg2rad = Math.PI / 180;
const rad2deg = 180 / Math.PI;
const bodies = Object.keys(sys);
const bodies_length = bodies.length;

export function calculateTrajectory(mission, timeout) { // calculate the trajectory of a spacecraft through the solar system
  return new Promise((resolve, reject) => {
    let p0 = performance.now();
    const dateStart = gre2jul(new Date(`${localStorage.getItem('firstEphemerisDay')}T00:00:00Z`));
    const julianInjectionDate = gre2jul(new Date(mission.start_day * 1000));
    let MET = [];
    let orbits = {};
    for (let b = 0; b < bodies_length; b++) {
      if (sys[bodies[b]].category !== null && !mission.included_bodies[sys[bodies[b]].category]) {
        continue;
      }
      orbits[bodies[b]] = {pos: []};
    }
    let shouldAbort = false;
    for (let t = 0; t < mission.end_day - mission.start_day; t += mission.frame_interval) {
      if (performance.now() - p0 > timeout * 1000) {
        shouldAbort = true;
        break;
      }
      MET.push(t);
      for (let b = 0; b < bodies_length; b++) {
        if (sys[bodies[b]].category !== null && !mission.included_bodies[sys[bodies[b]].category]) {
          continue;
        }
        const copernicianOutput = copernician(bodies[b], julianInjectionDate + t / 86400, dateStart);
        orbits[bodies[b]].pos.push(copernicianOutput.pos);
      }
    }
    resolve({
      orbits,
      MET,
      didAbort: shouldAbort,
      calcTime: performance.now() - p0,
      dateStart,
    });
  });
}

export function calculatePostKinematics(mission, kinematics, periapsisTarget) {
  // calculate full orbits
  function getTmly(body, parentBody, i) {
    let tmly = Math.atan2(
      kinematics.orbits[body].pos[i][1] - kinematics.orbits[parentBody].pos[i][1],
      kinematics.orbits[body].pos[i][0] - kinematics.orbits[parentBody].pos[i][0]
    );
    if (tmly < 0) {
      tmly += (2 * Math.PI);
    }
    return tmly;
  }
  let fullOrbits = {};
  for (let body in kinematics.orbits) {
    if (sys[body].category !== null && !mission.included_bodies[sys[body].category]) {
      continue;
    }
    fullOrbits[body] = [];
    const tmly0 = getTmly(body, sys[body].parent, 0);
    const tmlyF = getTmly(body, sys[body].parent, kinematics.MET.length-1);
    let hasCrossedVernal = false;
    let hasMadeFullCircle = false;
    for (let i=0; i<kinematics.MET.length; i+=1) {
      const tmly = getTmly(body, sys[body].parent, i);
      if (tmly < tmly0 && hasCrossedVernal === false) {
        hasCrossedVernal = true;
      }
      if (tmly > tmly0 && hasCrossedVernal === true) {
        hasMadeFullCircle = true;
        break;
      }
    }
    if (hasMadeFullCircle === false) {
      const lastTime = gre2jul(new Date(mission.start_day * 1000)) + (kinematics.MET[kinematics.MET.length-1] / 86400);
      const lastElements = getElementsForTime(body, lastTime, kinematics.dateStart);
      let tmlySpan;
      if (tmly0 < tmlyF) {
        tmlySpan = tmly0 + (2*Math.PI - tmlyF);
      }
      else {
        tmlySpan = tmly0 - tmlyF;
      }
      for (let tmlyA = 0; tmlyA<tmlySpan; tmlyA+=0.05) {
        const posvel = kep2car(
          lastElements[0],
          lastElements[1],
          lastElements[2],
          lastElements[3],
          lastElements[4] + tmlyA,
          lastElements[5] + tmlyA,
          sys[sys[body].parent].mu
        );
        fullOrbits[body].push(posvel.pos);
      }
    }
  }
  return ({
    fullOrbits,
  });
}

export function getElementsForTime(bod, tRaw, dateStart) {
  const dx = sys[bod].dx;
  const d = Math.floor((tRaw - dateStart)/dx); // index in sys entry of JPL Horizons data point datestamped just before tRaw
  const t0 = (((tRaw - dateStart)/dx) - d) * dx; // number of days between tRaw and the JPL Horizons point datestamped just before tRaw
  const t = [
    null,
    t0,
    Math.pow(t0,2),
    Math.pow(t0,3),
  ];

  let ltrl = [0, 0, 0, 0, 0, 0];
  for (let e = 5; e >= 0; e--) {
    const a = sys[bod].spln[e][d];
    ltrl[e] = a[0] + a[1]*t[1] + a[2]*t[2] + a[3]*t[3];
  }

  return ltrl;
}

export function copernician(bod, tRaw, dateStart) { // calculate the position and velocity of a celestial body
  const ltrl = getElementsForTime(bod, tRaw, dateStart);

  const posvel = kep2car(
    ltrl[0],
    ltrl[1],
    ltrl[2],
    ltrl[3],
    ltrl[4],
    ltrl[5],
    sys[sys[bod].parent].mu,
  );

  if (sys[bod].parent === 'sol') {
    return posvel;
  }
  else {
    const {pos: parent_pos} = copernician(sys[bod].parent, tRaw, dateStart);
    return {
      pos: [
        posvel.pos[0] + parent_pos[0],
        posvel.pos[1] + parent_pos[1],
        posvel.pos[2] + parent_pos[2],
      ],
    };
  }
}

export function kep2car(angm, eccn, rasc, incl, argptmly, tmly, mu) { // convert from keplerian elements to cartesian elements
  const para = (Math.pow(angm,2) / mu); // parameter of orbit, or semi-latus rectum
  const radi = para / (1 + eccn*Math.cos(tmly)); // radius from planet center

  const C_rasc = Math.cos(rasc);
  const S_rasc = Math.sin(rasc);
  const C_incl = Math.cos(incl);
  const S_incl = Math.sin(incl);
  const C_argptmly = Math.cos(argptmly);
  const S_argptmly = Math.sin(argptmly);

  const C = C_rasc * C_incl;
  const D = S_rasc * C_incl;

  return ({
    pos: [
      radi * (C_rasc * C_argptmly - D * S_argptmly),
      radi * (S_rasc * C_argptmly + C * S_argptmly),
      radi * S_incl * S_argptmly,
    ],
  });
}

export function equ2ecl4pointWithVernal(eclAxis, equ) { // convert a point from body-centric equatorial (celestial) coordinates to ecliptic while maintaining vernal direction
  const decl = eclAxis.decl * deg2rad;
  const rasc = eclAxis.rasc * deg2rad;
  const equatorNormal = [
    Math.cos(decl) * Math.cos(rasc),
    Math.cos(decl) * Math.sin(rasc),
    Math.sin(decl),
  ];

  // pitch the coordinates up around the ecliptic y-axis,
  // then roll them around the new x-axis
  // i.e. y * x' rotation
  const planeIntersectionDirection = cross([0,1,0],equatorNormal);
  const pitch = -Math.atan2(planeIntersectionDirection[2], planeIntersectionDirection[0]);
  const roll = -((Math.PI / 2) - Math.acos(dot([0,1,0],equatorNormal) / mag(equatorNormal)));

  return ([
    (Math.cos(pitch) * equ[0]) + ((Math.sin(pitch)*Math.sin(roll)) * equ[1]) + ((Math.sin(pitch)*Math.cos(roll)) * equ[2]),
    (Math.cos(roll) * equ[1]) + ((-Math.sin(roll)) * equ[2]),
    (-Math.sin(pitch) * equ[0]) + ((Math.cos(pitch)*Math.sin(roll)) * equ[1]) + ((Math.cos(pitch)*Math.cos(roll)) * equ[2]),
  ]);
}

function equ2ecl4point(eclAxis, point) { // convert a point from body-centric equatorial (celestial) coordinates to ecliptic
  const rasc = eclAxis.rasc * deg2rad;
  const decl = (90 - eclAxis.decl) * deg2rad;
  point = [
    (Math.cos(decl) * point[0] + Math.sin(decl) * point[2]),
    (point[1]),
    (-Math.sin(decl) * point[0] + Math.cos(decl) * point[2]),
  ];
  point = [
    (Math.cos(rasc) * point[0] - Math.sin(rasc) * point[1]),
    (Math.sin(rasc) * point[0] + Math.cos(rasc) * point[1]),
    (point[2]),
  ];
  return point;
}

function equ2ecl4axis(equAxis) { // convert an axis from earth equatorial (celestial) coordinates to ecliptic
  const rasc = equAxis.rasc * deg2rad;
  const decl = equAxis.decl * deg2rad;
  const equ = {
    x: Math.cos(decl) * Math.cos(rasc),
    y: Math.cos(decl) * Math.sin(rasc),
    z: Math.sin(decl),
  };
  const obli = sys['ter'].axis.obli * deg2rad;
  const ecl = {
    x: equ.x,
    y: Math.cos(obli) * equ.y + Math.sin(obli) * equ.z,
    z: -Math.sin(obli) * equ.y + Math.cos(obli) * equ.z,
  };
  return ({
    rasc: Math.atan2(ecl.y,ecl.x) * rad2deg,
    decl: 90 - Math.atan2(Math.sqrt(Math.pow(ecl.x,2) + Math.pow(ecl.y,2)), ecl.z) * rad2deg,
  });
}

function mmly2emly(mmly, eccn) { // convert from mean anomaly to eccentric anomaly
  const tol = 1e-8;
  let emly;
  if (mmly > Math.PI) {
    emly = mmly - eccn/2;
  }
  else {
    emly = mmly + eccn/2;
  }
  let dist = 1;
  while (dist > tol) {
    const f_over_fp = (emly - eccn*Math.sin(emly) - mmly) / (1 - eccn*Math.cos(emly));
    dist = Math.abs(f_over_fp);
    emly = emly - f_over_fp;
  }
  return (emly)
}

function unwind(dx, n, y, orbitsPerDay = null, unwoundArgptmly = null) {
  if (orbitsPerDay !== null) {
    // Treat moons differently because they have fast orbits
    // that may alias with dx.
    const radiansPerDx = orbitsPerDay * tau * dx;
    let winds = 0;
    for (let i=1; i<n; i++) {
      y[i] += winds;
      while (y[i] < y[i-1]) {
        y[i] += tau;
        winds += tau;
      }
      while ((y[i] - y[i-1]) < (radiansPerDx * 0.8)) {
        y[i] += tau;
        winds += tau;
      }
    }
  }
  else {
    let winds = 0;
    for (let i=1; i<n; i++) {
      y[i] += winds;
      if (Math.abs(y[i] - y[i-1]) > 3.1) {
        if (y[i] > y[i-1]) {
          y[i] -= tau;
          winds -= tau;
        }
        else {
          y[i] += tau;
          winds += tau;
        }
      }
      if (!!unwoundArgptmly) {
        const a = unwoundArgptmly[i-1] + y[i-1];
        const b = unwoundArgptmly[i] + y[i];
        if (b < a) {
          y[i] += tau;
          winds += tau;
        }
      }
    }
  }

  return y;
}

export function cubIntpl(dx,n,c,y) { // calculate natural cubic splines to interpolate twixt JPL Horizons data
  const dx2 = Math.pow(dx,2);

  let r = [];
  for (let i=0; i<n; i++) { // assemble r, where m * k = r
    if (i === 0) {
      r[i] = 3 * (y[1] - y[0]) / dx2;
    }
    else if (i === n-1) {
      r[i] = 3 * (y[n-1] - y[n-2]) / dx2;
    }
    else {
      r[i] = 3 * (y[i+1] - y[i-1]) / dx2;
    }
  }

  let d = []; // perform tridiagonal matrix algorithm
  for (let i=0; i<n; i++) {
    if (i === 0) {
      d[i] = r[0] / getM(0,0,n,dx);
    }
    else {
      d[i] = (r[i] - getM(i,i-1,n,dx) * d[i-1]) / (getM(i,i,n,dx) - getM(i,i-1,n,dx) * c[i-1]);
    }
  }
  let k = [];
  for (let i=n-1; i>=0; i--) {
    if (i === n-1) {
      k[i] = d[i];
    }
    else {
      k[i] = d[i] - c[i] * k[i+1];
    }
  }

  let a = [];
  for (let i=0; i<n-1; i++) {
    const dy = y[i+1] - y[i];
    a[i] = [
      (y[i]),
      (k[i]),
      ((((-k[i] - k[i+1] + 2*dy/dx) / dx) - ((k[i]*dx - dy) / dx2))),
      (((k[i] + k[i+1] - 2*dy/dx) / dx2)),
    ];
  }

  return a;
}

function toPrecision(number, precision = 4) {
  return Number(number.toPrecision(4));
}

function getM(i, j, n, dx) { // assemble m, where m * k = r
  if (i === 0) {
    if (j === 0) {
      return 2 / dx;
    }
    else if (j === 1) {
      return 1 / dx;
    }
    else {
      return 0;
    }
  }
  else if (i === n-1) {
    if (j === n-1) {
      return 2 / dx;
    }
    else if (j === n-2) {
      return 1 / dx;
    }
    else {
      return 0;
    }
  }
  else {
    if (j === i-1) {
      return 1 / dx;
    }
    else if (j === i) {
      return 4 / dx;
    }
    else if (j === i+1) {
      return 1 / dx;
    }
    else {
      return 0;
    }
  }
}

export function mag(vector) {
  let radical = 0;
  for (let a=0; a<3; a++) {
    radical += (Math.pow(vector[a],2));
  }
  return (Math.sqrt(radical));
}

export function norm(vector) {
  let unit = [0, 0, 0];
  let magVal = mag(vector);
  for (let a=0; a<3; a++) {
    unit[a] = vector[a] / magVal;
  }
  return (unit);
}

export function avg(x) {
  let avg = 0;
  for (let i=0; i<x.length; i++) {
    avg += x[i];
  }
  avg /= x.length;
  return avg;
}

export function cross(a, b) {
  return ([a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0]]);
}

export function dot(a, b) {
  return (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}

export function jul2gre(J) {
  const y = 4716;
  const j = 1401;
  const m = 2;
  const n = 12;
  const r = 4;
  const p = 1461;
  const v = 3;
  const u = 5;
  const s = 153;
  const w = 2;
  const B = 274277;
  const C = -38;

  const f = J + j + Math.floor((Math.floor((4 * J + B) / 146097) * 3) / 4) + C;
  const e = r * f + v;
  const g = Math.floor((e % p) / r);
  const h = u * g + w;

  const day = Math.floor((h % s) / u) + 1;
  const month = ((Math.floor(h / s) + m) % n) + 1;
  const year = Math.floor(e / p) - y + Math.floor((n + m - month) / n);
  const hour = Math.floor(((J - 0.5) % 1) * 24);
  const minute = Math.floor((J % (1/24)) * 1440);
  const second = Math.floor((J % (1/1440)) * 86400);
  const millisecond = Math.floor((J % (1/1440000)) * 86400000);

  let d = new Date();
  d.setUTCFullYear(year);
  d.setUTCMonth(month - 1);
  d.setUTCDate(day - 1);
  d.setUTCHours(hour);
  d.setUTCMinutes(minute);
  d.setUTCSeconds(second);
  d.setUTCMilliseconds(millisecond);

  return (d);
}

export function gre2jul(date) {
  const Y = date.getFullYear();
  const M = date.getMonth() + 1;
  const D = date.getDate();
  const h = date.getUTCHours();
  const m = date.getUTCMinutes();
  const s = date.getUTCSeconds();

  const X = Math.ceil((M - 14) / 12);
  const A = Math.floor((1461 * (Y + 4800 + X)) / 4);
  const B = Math.floor((367 * (M - 2 - 12 * X)) / 12);
  const C = Math.floor((3 * Math.floor((Y + 4900 + X) / 100)) / 4);

  let N = A + B - C + D - 32075;
  if (h < 12) {
    N++;
  }
  N += ((h - 12) / 24);
  N += (m / 1440);
  N += (s / 86400);

  return N;
}

export function parseHorizonsData(rawString, curBody, dx) {
  return new Promise((resolve, reject) => {
    let raw = rawString.split('\n');
    raw = raw.slice(raw.findIndex(r => r === '$$SOE')+1,raw.findIndex(r => r === '$$EOE'));
    const elmts = [[],[],[],[],[],[]];
    let orbitsPerDay;
    for (let i=0; i<raw.length; i+=5) {
      const mu = sys[sys[curBody].parent].mu;
      const eccn = Number(raw[i+1].substring( 4,26));
      const incl = Number(raw[i+1].substring(57,78)) * deg2rad;
      const rasc = Number(raw[i+2].substring( 4,26)) * deg2rad;
      const argp = Number(raw[i+2].substring(31,52)) * deg2rad;
      const tmly = Number(raw[i+3].substring(57,78)) * deg2rad;
      const semi = Number(raw[i+4].substring( 4,26));
      const rada = semi * (1+eccn);
      const radp = semi * (1-eccn);
      const angm = Math.sqrt(2*mu) * Math.sqrt((rada*radp)/(rada+radp));
      elmts[0].push(angm);
      elmts[1].push(eccn);
      elmts[2].push(rasc);
      elmts[3].push(incl);
      elmts[4].push(argp+tmly);
      elmts[5].push(tmly);
      if (i === 0) {
        orbitsPerDay = Number(raw[i+3].substring( 4,26)) * 86400 / 360;
      }
    }

    elmts[4] = elmts[4].map(elmt => elmt % tau);

    // calculate natural cubic spline interpolations of JPL Horizons data points
    const n = elmts[0].length;
    let c = [];
    for (let i=0; i<n-1; i++) {
      if (i === 0) {
        c[i] = getM(0,1,n,dx) / getM(0,0,n,dx);
      }
      else {
        c[i] = getM(i,i+1,n,dx) / (getM(i,i,n,dx) - getM(i,i-1,n,dx) * c[i-1]);
      }
    }

    sys[curBody].spln = Array(6);
    sys[curBody].spln[0] = cubIntpl(dx, n, c, elmts[0]);
    sys[curBody].spln[1] = cubIntpl(dx, n, c, elmts[1]);
    sys[curBody].spln[3] = cubIntpl(dx, n, c, unwind(dx, n, elmts[3].slice()));
    const unwoundArgptmly = unwind(dx, n, elmts[4].slice(), sys[curBody].parent !== 'sol' ? orbitsPerDay : undefined);
    sys[curBody].spln[4] = cubIntpl(dx, n, c, unwoundArgptmly);
    sys[curBody].spln[2] = cubIntpl(dx, n, c, unwind(dx, n, elmts[2].slice(), undefined, curBody === 'ter' ? unwoundArgptmly : undefined));
    sys[curBody].spln[5] = cubIntpl(dx, n, c, unwind(dx, n, elmts[5].slice()));

    sys[curBody].dx = dx;
    resolve();
  });
}

export function assembleWireframes(curBody) {
  const axisInEclipticFrame = equ2ecl4axis({
    rasc: sys[curBody].axis.rasc,
    decl: sys[curBody].axis.decl,
  })
  sys[curBody].grid = {
    lat: [],
    lon: [],
  }
  for (let phi=30; phi<=150; phi+=30) {
    var lat_line = [];
    for (let theta=0; theta<=360; theta+=5) {
      const p = phi * deg2rad;
      const t = theta * deg2rad;
      lat_line.push(equ2ecl4point(axisInEclipticFrame, [
        sys[curBody].rad * Math.sin(p) * Math.cos(t),
        sys[curBody].rad * Math.sin(p) * Math.sin(t),
        sys[curBody].rad * Math.cos(p),
      ]));
    }
    sys[curBody].grid.lat.push(lat_line);
  }
  for (let theta=0; theta<360; theta+=30) {
    let lon_line = [];
    for (let phi=0; phi<=180; phi+=5) {
      const p = phi * deg2rad;
      const t = theta * deg2rad;
      lon_line.push(equ2ecl4point(axisInEclipticFrame, [
        sys[curBody].rad * Math.sin(p) * Math.cos(t),
        sys[curBody].rad * Math.sin(p) * Math.sin(t),
        sys[curBody].rad * Math.cos(p),
      ]));
    }
    sys[curBody].grid.lon.push(lon_line);
  }
}
